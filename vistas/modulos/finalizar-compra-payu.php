<?php

require dirname(__DIR__) .'/..' . '/extensiones/vendor/openpay/sdk/Openpay.php';
require_once dirname(__DIR__) .'/..' . '/extensiones/PHPMailer/PHPMailerAutoload.php';

$tabla = "comercio";

$respuesta = ModeloCarrito::mdlMostrarTarifas($tabla);

$idOpenPay = $respuesta["accountIdPayu"];
$llavePrivadaOpenPay = $respuesta["apiKeyPayu"];
$llavePublicaOpenPay = $respuesta["merchantIdPayu"];
$modoOpenPay = $respuesta["modoPayu"];
session_start();

// if(!isset($_SESSION["validarSesion"]))
// {

//     echo '<script>

    

//     </script>';
   
// }


/*=============================================
PAGO OPENPAY
=============================================*/
if($_GET['openpay'] === 'true' && isset( $_GET["id"]))
{        
    if($modoOpenPay === 'sandbox')
    {
        Openpay::setProductionMode(false); 
        Openpay::setId($idOpenPay);
        Openpay::setApiKey($llavePrivadaOpenPay);
        }
    else
    {
        Openpay::setProductionMode(true); 
        Openpay::setId($idOpenPay);
        Openpay::setApiKey($llavePrivadaOpenPay);
            
    }
     
   $openpay = Openpay::getInstance($idOpenPay,
   $llavePrivadaOpenPay);

   

   #recibo los productos comprados
   $productos = explode("-", $_GET['productos']);
   $cantidad = explode("-", $_GET['cantidad']);
   $pago = explode("-", $_GET['pago']); 
   $colores = explode("-", $_GET['colores']); 
   $tallas = explode("-", $_GET['tallas']); 

   #capturamos el Id del pago que arroja Paypal
   $paymentId = $_GET['id'];

   #Creamos un objeto de charge para confirmar que las credenciales si tengan el Id de pago resuelto
   #$payment = Payment::get($paymentId, $apiContext);
   
   $charge = $openpay->charges->get($paymentId);
   $estatusTransaccion = $charge->status;
   $fechaActual = date('Y-m-d');
   
   if($estatusTransaccion === 'completed')
   {     
      #Insertamos un nuevo pedido para la paqueteria (PAKKE)
      $datos = array("CourierCode"=>$_SESSION["CourierCode"],
                        "CourierName"=>$_SESSION["CourierName"],
                        "CourierServiceId"=>$_SESSION["CourierServiceId"],
                        "CourierServiceName"=>$_SESSION["CourierServiceName"],
                        "DeliveryDays"=>$_SESSION["DeliveryDays"],
                        "EstimatedDeliveryDate"=>$_SESSION["EstimatedDeliveryDate"],
                        "TotalPrice"=>$_SESSION["TotalPrice"],
                         "ResellerServiceConfigId"=>"-",
                         "No_Guia"=>"0",
                         "fecha"=>$fechaActual,
                         "estatus"=>"confirmado",
                         "ShipmentId"=>"0",
                         "calle_numero"=>$_SESSION["direccionCliente"],
                         "colonia"=>$_SESSION["coloniaCliente"],
                         "cp"=>$_SESSION["cpCliente"],
                         "ciudad"=>$_SESSION["ciudadCliente"],
                         "estado"=>$_SESSION["estadoCliente"]
                        );

      $No_Pedido = ControladorCarrito::ctrNuevosPedidos($datos);
          

      if($No_Pedido>0){
        #Actualizamos la base de datos
            for($i = 0; $i < count($productos); $i++){
        
                $datos = array("idUsuario"=>$_SESSION["id"],
                            "idProducto"=>$productos[$i],
                            "talla"=>$tallas[$i],
                            "color"=>$colores[$i],
                            "metodo"=>"openpay",
                            "email"=>$_SESSION["email"],
                            "direccion"=>$_SESSION["direccionCliente"],
                            "pais"=>"Mexico",
                            "No_Pedido"=>$No_Pedido,
                            "pago"=>$pago[$i]);

                $respuesta = ControladorCarrito::ctrNuevasCompras($datos);

                $ordenar = "id";
                $item = "id";
                $valor = $productos[$i];

                $productosCompra = ControladorProductos::ctrListarProductos($ordenar, $item, $valor);

                foreach ($productosCompra as $key => $value) {

                $item1 = "ventas";
                $valor1 = $value["ventas"] + $cantidad[$i];

                $item2 = "stock";
                $valor2 = $value["stock"] - $cantidad[$i];

                $item3 = "id";
                $valor3 =$value["id"];
                

                $actualizarCompra = ControladorProductos::ctrActualizarProductos($item1, $valor1, $item2, $valor2, $item3, $valor3);
                ControladorProductos::ctrBorrarCarritoOlvidao("usuario", $_SESSION["email"]);
                if(isset($_SESSION["id"]))
                    ControladorUsuarios::ctrActualizarUsuario($_SESSION["id"],descuento_primera_compra,"N");
                }

                if($respuesta == "ok" && $actualizarCompra == "ok"){

                    try {
                        enviarCorreoUsuario(); 
                        enviarCorreoAdministrador();
                    }
                    catch (Exception $e) {
                        echo 'Excepción capturada: ',  $e->getMessage(), "\n";
                    }
                echo '<script>


                if (localStorage.getItem("listaProductos") !== null) {

                    var listaCarrito = JSON.parse(localStorage.getItem("listaProductos"));
                
                    listaCarrito.forEach(funcionForEach);
                
                    function funcionForEach(item, index) {
                
                        var titulo = item.titulo;
                        //var categoria = $(this).attr("categoria");
                        //var subcategoria = $(this).attr("subcategoria");
                        var precio = item.precio;
                        var sku = item.sku;
              
                fbq("track", "Purchase", {
                    content_name: titulo,
                    //content_category: categoria,
                    content_ids:"["+sku+"]",
                    content_type: "product", 
                    value: precio,
                    currency: "MXN"
              
                });
              
                    }
              
                }


                localStorage.removeItem("listaProductos");
                localStorage.removeItem("cantidadCesta");
                localStorage.removeItem("sumaCesta");

                swal({
                    title: "",
                    text: "¡Operación completada!",
                    type: "success",
                    closeOnConfirm: false
                },
                function(isConfirm){
                    if (isConfirm) {	   
                        window.location = "'.$url.'perfil";
                    } 
            });
                </script>';

                }
            }
      }
      
   }
   else{
      $motivoError;

      if($charge->error_code != null){

         switch ($charge->error_code) {
            case 3001:
            $motivoError = "La tarjeta fue declinada";
                break;
            case 3002:
            $motivoError = "La tarjeta ha expirado.";
                break;
            case 3003:
            $motivoError = "La tarjeta no tiene fondos suficientes";
                break;
                case 3004:
            $motivoError = "La tarjeta ha sido identificada como una tarjeta robada.";
                break;
                case 3005:
            $motivoError = "La tarjeta ha sido rechazada por el sistema antifraudes.";
                break;
                case 3006:
            $motivoError = "La operación no esta permitida para este cliente o esta transacción";
                break;
                case 3007:
            $motivoError = "Deprecado. La tarjeta fue declinada.";
                break;
                case 3008:
            $motivoError = "La tarjeta no es soportada en transacciones en línea.";
                break;
                case 3009:
            $motivoError = "La tarjeta fue reportada como perdida.";
                break;
                case 3010:
            $motivoError = "El banco ha restringido la tarjeta.";
                break;
                case 3011:
            $motivoError = "El banco ha solicitado que la tarjeta sea retenida. Contacte al banco";
                break;
                case 3012:
            $motivoError = "Se requiere solicitar al banco autorización para realizar este pago.";
                break;
        }

         echo '<script>
         swal({
            title: "Tarjeta rechazada",
            text: "'.$motivoError.'",
            type: "warning",
            closeOnConfirm: false
          },
          function(isConfirm){
              if (isConfirm) {	   
                   window.location = "'.$url.'carrito-de-compras";
              } 
      });         
   
        </script>';
      }
   
   }
}

function enviarCorreoUsuario(){
	//enviar
    /*=============================================
    VERIFICACIÓN CORREO ELECTRÓNICO
    =============================================*/

    date_default_timezone_set("America/Mexico_City");

    $mail = new PHPMailer;

    $mail->CharSet = 'UTF-8';

    $mail->isMail();

    $mail->setFrom('tienda@yupashop.com', 'Yupashop');

    $mail->addReplyTo('tienda@yupashop.com', 'Yupashop');

    $mail->Subject = "Compra realizada";

    $mail->addAddress($_SESSION["email"]);
    $mail->msgHTML('<div style="width:100%; background:#eee; position:relative; font-family:sans-serif; padding-bottom:40px">
        
        <center>
            
            <img style="padding:20px; width:10%" src="https://www.yupashop.com/tienda/logo.png">

        </center>

        <div style="position:relative; margin:auto; width:600px; background:white; padding:20px">
        
            <center>
            
            <img style="padding:20px; width:15%" src="https://www.yupashop.com/tienda/icon-email.png">

            <h3 style="font-weight:100; color:#999">TU COMPRA HA SIDO REGISTRADA</h3>

            <hr style="border:1px solid #ccc; width:80%">

            <h4 style="font-weight:100; color:#999; padding:0 20px">Se te mandará un correo con el número de guia para el rastreo de tu pedido</h4>

            <div style="line-height:60px; background:#0aa; width:60%; color:white">Gracias por comprar en Yupashop</div>

            </a>

            <br>

            </center>

        </div>

    </div>');

    $envio = $mail->Send();
    
				
	}


    function enviarCorreoAdministrador(){
        //enviar
                    /*=============================================
                    VERIFICACIÓN CORREO ELECTRÓNICO
                    =============================================*/
                    date_default_timezone_set("America/Mexico_City");
    
                    $mailo = new PHPMailer;
    
                    $mailo->CharSet = 'UTF-8';
    
                    $mailo->isMail();
    
                    $mailo->setFrom('unanuevacompra@yupashop.com', 'Yupashop');
    
                    $mailo->addReplyTo($_SESSION["email"], 'Yupashop');
    
                    $mailo->Subject = "Se ha concretado una nueva compra!";
    
                    $mailo->addAddress('tienda@yupashop.com');
    
                    $mailo->msgHTML('<div style="width:100%; background:#eee; position:relative; font-family:sans-serif; padding-bottom:40px">
                        
                        <center>
                            
                            <img style="padding:20px; width:10%" src="https://www.yupashop.com/tienda/logo.png">
    
                        </center>
    
                        <div style="position:relative; margin:auto; width:600px; background:white; padding:20px">
                        
                            <center>
                            
                            <img style="padding:20px; width:15%" src="https://www.yupashop.com/tienda/icon-email.png">
    
                            <h3 style="font-weight:100; color:#999">NUeva compra</h3>
    
                            <hr style="border:1px solid #ccc; width:80%">
    
                            <h4 style="font-weight:100; color:#999; padding:0 20px">Usuario '.$_SESSION["email"].'</h4>
                            <h4 style="font-weight:100; color:#999; padding:0 20px">Direccion '.$_SESSION["direccionCliente"].'</h4>
    
                            </a>
    
                            <br>
    
                            </center>
    
                        </div>
    
                    </div>');
    
                    $envio = $mailo->Send();
        }