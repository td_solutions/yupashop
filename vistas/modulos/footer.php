<!--=====================================
FOOTER
======================================-->

<footer class="container-fluid">

	<div class="container">
	<!--=====================================
	RENGLON UNICO DE INFROMES
	======================================-->
	<div class="row principal_contactenos" formContacto">
		<!--=====================================
		COLUMNA DE DIRECCION DE TIENDA
		======================================-->
		<div class="col-md-3 col-sm-6 col-xs-12 text-left infoContacto">
		<div class="row">
				<div class="col-md-12">
				<h5>Dudas e inquietudes, contáctenos en:</h5>
				</div>	
				<div class="col-md-12 infoContacto">
				<h5><i class="fa fa-phone-square" aria-hidden="true"></i> 36670 tel oficina. 4621732966</h5>
				</div>
				<div class="col-md-12 infoContacto">
				<h5><i class="fa fa-envelope" aria-hidden="true"></i> tienda@yupashop.com</h5>
				</div>	
				<div class="col-md-12 infoContacto">
				<h5><i class="fa fa-map-marker" aria-hidden="true"></i> Plaza de las fuentes 6273 int 49</h5>
				</div>
				<div class="col-md-12 infoContacto">
				<h5>Irapuato | Guanajuato</h5>
				</div>
			</div>		
		</div>

		<!--=====================================
		COLUMNA DEL MAPA
		======================================-->
		<div class="col-md-3 col-sm-6 col-xs-12 text-left infoContacto">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d233.2851801467231!2d-101.40856453571968!3d20.687355305782862!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842c7f151e75b96f%3A0xbea09a4bf0550798!2sPaseo+de+las+Fuentes+6273%2C+Villas+de+Irapuato%2C+Gto.!5e0!3m2!1ses!2smx!4v1559150210578!5m2!1ses!2smx" width="100%" height="200" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
		<!--=====================================
		FORMULARIO CONTÁCTENOS
		======================================-->
		<div class="col-md-6 col-sm-6 col-xs-12 text-left infoContacto">
		<h5>RESUELVA SU INQUIETUD</h5>
		<form role="form" method="post" onsubmit="return validarContactenos()">
			<div class="col-md-6 col-sm-6 col-xs-6">
			<input type="text" id="nombreContactenos" name="nombreContactenos" class="form-control" placeholder="Escriba su nombre" required>
			<br>
			<input type="email" id="emailContactenos" name="emailContactenos" class="form-control" placeholder="Escriba su correo electrónico" required>  
			</div>
			<div class="col-md-6 col-sm-6 col-xs-6">
				<textarea id="mensajeContactenos" name="mensajeContactenos" class="form-control" placeholder="Escriba su mensaje" rows="5" required></textarea>
				<br>
	      <input type="submit" value="Enviar" class="btn btn-default backColor pull-right" id="enviar">
			</div>
			</form>

				<?php 

					$contactenos = new ControladorUsuarios();
					$contactenos -> ctrFormularioContactenos();

				?>
	</div>
</footer>


<div class="modal fade modalFormulario" id="modalTerminos" role="dialog">
<button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-dialog">
		<iframe src="https://www.yupashop.com/tienda/terminos-y-condiciones.html" style="border:none;" width="100%" height="790" seamless disabled></iframe>
	</div>
</div>

<div class="modal fade modalFormulario" id="modalPoliticas" role="dialog">
<button type="button" class="close" data-dismiss="modal">&times;</button>
    <div class="modal-content modal-dialog">
		<iframe src="https://www.yupashop.com/tienda/aviso-de-privacidad.html" style="border:none;" width="100%" height="790" seamless disabled></iframe>
	</div>
</div>


<!--=====================================
FINAL 
======================================-->

<div class="container-fluid final">
	
	<div class="container">
	
		<div class="row">
			
			<div class="col-sm-6 col-xs-12 text-left text-muted">
				
				<h5>&copy; 2019 Yupa Shop. Todos los derechos reservados.</h5>

			</div>

			<div class="col-sm-6 col-xs-12 text-right social">
				
			<ul>
				<li class="nav_first">
				<a href="#modalTerminos" data-toggle="modal">Términos y condiciones</a>
				</li>
				<li class="nav_first">
				<a href="#modalPoliticas" data-toggle="modal">Aviso de privacidad</a>
				</li>
			<?php
				
			$social = ControladorPlantilla::ctrEstiloPlantilla();

				$jsonRedesSociales = json_decode($social["redesSociales"],true);		

				foreach ($jsonRedesSociales as $key => $value) {
					if($value["activo"] != 0){
					echo '<li>
							<a href="'.$value["url"].'" target="_blank">
								<i class="fa '.$value["red"].' redSocial '.$value["estilo"].'" aria-hidden="true"></i>
							</a>
						</li>';
					}
				}

			?>

			</ul>

			</div>

		</div>

	</div>

</div>