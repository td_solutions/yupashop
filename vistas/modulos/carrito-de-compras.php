<?php
    
    $url = Ruta::ctrRuta();

 ?>

<!--=====================================
BREADCRUMB CARRITO DE COMPRAS
======================================-->

<div class="container-fluid well well-sm">
	
	<div class="container">
		
		<div class="row">
			
			<ul class="breadcrumb fondoBreadcrumb text-uppercase">
				
				<li><a href="<?php echo $url;  ?>">CARRITO DE COMPRAS</a></li>
				<li class="active pagActiva"><?php echo $rutas[0] ?></li>

			</ul>

		</div>

	</div>

</div>

<!--=====================================
TABLA CARRITO DE COMPRAS
======================================-->

<div class="container-fluid">

	<div class="container">

		<div class="panel panel-default">
			
			<!--=====================================
			CABECERA CARRITO DE COMPRAS
			======================================-->

			<div class="panel-heading cabeceraCarrito">
				
				<div class="col-md-6 col-sm-7 col-xs-12 text-center">
					
					<h3>
						<small>PRODUCTO</small>
					</h3>

				</div>

			<div class="col-md-2 col-sm-1 col-xs-0 text-center">
					
					<h3>
						<small>PRECIO</small>
					</h3>

				</div>

				<div class="col-sm-2 col-xs-0 text-center">
					
					<h3>
						<small>CANTIDAD</small>
					</h3>

				</div>

				<div class="col-sm-2 col-xs-0 text-center">
					
					<h3>
						<small>SUBTOTAL</small>
					</h3>

				</div>

			</div>

			<!--=====================================
			CUERPO CARRITO DE COMPRAS
			======================================-->

			<div class="panel-body cuerpoCarrito">

				

			</div>

			<!--=====================================
			SUMA DEL TOTAL DE PRODUCTOS
			======================================-->

			<div class="panel-body sumaCarrito">

				<div class="col-md-4 col-sm-6 col-xs-12 pull-right well">
					
					<div class="col-xs-6">
						
						<h4>TOTAL:</h4>

					</div>

					<div class="col-xs-6">

						<h4 class="sumaSubTotal">
							
							

						</h4>

					</div> 

				</div>

			</div>

			<!--=====================================
			BOTÓN CHECKOUT
			======================================-->

			<div class="panel-heading cabeceraCheckout">

			<?php

				if(isset($_SESSION["validarSesion"])){

					if($_SESSION["validarSesion"] == "ok"){

						echo '<a id="btnCheckout" href="#modalCheckout" data-toggle="modal" idUsuario="'.$_SESSION["id"].'"><button class="btn btn-default btn-lg pull-right">REALIZAR PAGO</button></a>';

					}


				}else{

					echo '<a href="#modalIngreso" data-toggle="modal"><button class="btn btn-default btn-lg pull-right">REALIZAR PAGO</button></a>';
				}

			?>	

			</div>
		</div>
	</div>
</div>

<!--=====================================
VENTANA MODAL PARA CHECKOUT
======================================-->

<div id="modalCheckout" class="modal fade modalFormulario modalRealizarEnvioPago" role="dialog" style="overflow-y: scroll;z-index: 1400;">
	
	 <div class="modal-content modal-dialog">
	 	
		<div class="modal-body modalTitulo">
			
			<h3 id="titulo_realizar_pago"class="backColor">REALIZAR PAGO 1/3</h3>

			<button type="button" class="close" data-dismiss="modal">&times;</button>

			<div class="contenidoCheckout">

				<?php

				$respuesta = ControladorCarrito::ctrMostrarTarifas();

				echo '<input type="hidden" id="tasaImpuesto" value="'.$respuesta["impuesto"].'">
					  <input type="hidden" id="envioNacional" value="'.$respuesta["envioNacional"].'">
				      <input type="hidden" id="envioInternacional" value="'.$respuesta["envioInternacional"].'">
				      <input type="hidden" id="tasaMinimaNal" value="'.$respuesta["tasaMinimaNal"].'">
				      <input type="hidden" id="tasaMinimaInt" value="'.$respuesta["tasaMinimaInt"].'">
							<input type="hidden" id="tasaPais" value="'.$respuesta["pais"].'">
							
							<input type="hidden" id="idOpenPay" value="'.$respuesta["accountIdPayu"].'">
							<input type="hidden" id="llavePrivadaOpenPay" value="'.$respuesta["apiKeyPayu"].'">
							<input type="hidden" id="llavePublicaOpenPay" value="'.$respuesta["merchantIdPayu"].'">
							<input type="hidden" id="modoOpenPay" value="'.$respuesta["modoPayu"].'">

							
							<input type="hidden" id="CourierCode">
							<input type="hidden" id="CourierName">
							<input type="hidden" id="CourierServiceId">
							<input type="hidden" id="CourierServiceName">
							<input type="hidden" id="DeliveryDays">
							<input type="hidden" id="EstimatedDeliveryDate">
							<input type="hidden" id="TotalPrice">
							<input type="hidden" id="ResellerServiceConfigId">

				';

				?>
				<div class="container-fluid calculoEnvio">					
					<div class="formEnvio row" style="display:none">
						<h4 class="text-center text-muted text-uppercase tituloModalCheckout">Cálculo de envío</h4>
						<hr>
						<div class="col-md-3 col-sm-3 col-xs-12">
							
							<div class="form-group">
    						<label for="pakke_length">Longitud</label>
    						<input type="text" class="form-control input-sm soloNumeros" id="pakke_length" placeholder="0.0 m">
  						</div>

						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">

							<div class="form-group">
    						<label for="pakke_width">Ancho</label>
    						<input type="text" class="form-control input-sm soloNumeros" id="pakke_width" placeholder="0.0 m">
  						</div>

						</div>
						<div class="col-md-3 col-sm-3 col-xs-12">

							<div class="form-group">
    						<label for="pakke_height">Alto</label>
    						<input type="text" class="form-control input-sm soloNumeros" id="pakke_height" placeholder="0.0 m">
  						</div>
						
						</div>

						<div class="col-md-3 col-sm-3 col-xs-12">
							
							<div class="form-group">
    						<label for="pakke_weight">Peso</label>
    						<input type="text" class="form-control input-sm soloNumeros" id="pakke_weight" placeholder="0.0 kg" disabled>
  						</div>

						</div>

					</div>

					<div class=" row">
						

					</div>

					<div class="row">
						<div class="col-md-8 col-sm-8 col-xs-12">

							<div class="form-group">
									<label for="pakke_adress">Direcci&oacute;n</label>
									<input type="text" class="form-control input-sm" id="pakke_adress" placeholder="Calle y N&uacute;mero" >
							</div>

						</div>
						<div class="col-md-4 col-sm-4 col-xs-12">
							
							<div class="form-group">
									<label for="pakke_zipCodeTo">C&oacute;digo postal</label>
									<input type="text" class="form-control input-sm soloNumeros" id="pakke_zipCodeTo" placeholder="C&oacute;digo postal">
							</div>

						</div>					
					</div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">

							<div class="form-group">
									<label for="pakke_colonia">Colonia</label>
									<input type="text" class="form-control input-sm" id="pakke_colonia" placeholder="Nombre de la colonia">
							</div>

						</div>											
					</div>

					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-12">

							<div class="form-group">
									<label for="pakke_ciudad">ciudad</label>
									<input type="text" class="form-control input-sm" id="pakke_ciudad" placeholder="Ciudad" >
							</div>

						</div>
						<div class="col-md-6 col-sm-6 col-xs-12">

							<div class="form-group">
									<label for="pakke_estado">Estado</label>
									<input type="text" class="form-control input-sm" id="pakke_estado" placeholder="Estado" >
							</div>

						</div>

					</div>
				<br>



					<button id="btn_calcular_costo_envio"class="btn btn-block btn-lg btn-default">Calcular Costo Envio</button>
					
					<div class="row">
					<h4 class="text-center text-muted text-uppercase tituloModalCheckout">Elige la paqueteria</h4>
					<hr>
					
					<div class="row">
  <div class="col-4">
    <div id="lista_paqueteria" class="list-group" role="tablist">
      
    </div>
  </div>  
</div>

			</div>

				</div>

		<div class="formaPagoyProductosaComprar">
				<div class="formaPago row" style="display:none">

					<input type="hidden" id="idUsuario" value="<?php echo $_SESSION["id"]; ?>
					
					<h4 class="text-center tituloModalCheckout text-muted text-uppercase">Elige la forma de pago</h4>
					<hr>
					<!-- <figure class="col-xs-6">
						<center>
							<input id="checkPaypal" type="radio" name="pago" value="paypal" checked>
							</center>
							<img src="<?php echo $url; ?>vistas/img/plantilla/paypal.jpg" class="img-thumbnail">
					</figure> -->
					<figure class="col-xs-6">
						<center>
						<input id="checkPayu" type="radio" name="pago" value="payu" cheked>
						</center>
							<img src="<?php echo $url; ?>vistas/img/plantilla/payu.jpg" class="img-thumbnail">
					</figure>

				</div>

				<br>

				<div class="listaProductos row">
					
					<h4 class="text-center tituloModalCheckout text-muted text-uppercase">Productos a comprar</h4>
					<hr>
					<table class="table table-striped tablaProductos">
						
						 <thead>
						 	
							<tr>		
								<th>Producto</th>
								<th>Cantidad</th>
								<th>Talla</th>
								<th>Color</th>
								<th>Precio</th>
							</tr>

						 </thead>

						 <tbody>
						 	


						 </tbody>

					</table>

					<div class="col-sm-6 col-xs-12 pull-right">
						
						<table class="table table-striped tablaTasas">
							
							<tbody>
								
								<tr>
									<td>Subtotal</td>	
									<td><span class="cambioDivisa">MXN</span> $<span class="valorSubtotal" valor="0">0</span></td>	
								</tr>

								<tr>
									<td>Envío</td>	
									<td><span class="cambioDivisa">MXN</span> $<span class="valorTotalEnvio" valor="0">0</span></td>	
								</tr>

								<tr>
									<td>Impuesto</td>	
									<td><span class="cambioDivisa">MXN</span> $<span class="valorTotalImpuesto" valor="0">0</span></td>	
								</tr>
								<tr id="tr_descuento_primera_compra" style="font-weight:bold;color:red;display:none">
									<td>Descuento</td>	
									<td><span class="cambioDivisa">MXN</span> $<span class="valorDescuentoPrimeraCompra" valor="0">0</span></td>	
								</tr>
								<tr>
									<td><strong>Total</strong></td>	
									<td><strong><span class="cambioDivisa">MXN</span> $<span class="valorTotalCompra" valor="0">0</span></strong></td>	
								</tr>

							</tbody>	

						</table>

						

					</div>

					<div class="clearfix"></div>					

					<button class="btn btn-block btn-lg btn-default btnPagarOpenPay purchase">Proceder al pago</button>

				</div>

			</div>

		</div>
	</div>

	<form action="#" method="POST" id="payment-form" style="display:none">
						<input type="hidden" name="token_id" id="token_id">
						<input type="hidden" name="deviceIdHiddenFieldName" id="deviceIdHiddenFieldName">
						<input type="hidden" name="use_card_points" id="use_card_points" value="false">
						<input type="hidden" name="amount" id="amount">
						<input type="hidden" name="description" id="description">
						<input type="hidden" name="name" id="name">
						<input type="hidden" name="last_name" id="last_name">
						<input type="hidden" name="phone_number" id="phone_number">
						<input type="hidden" name="email" id="email">
						<div class="pymnt-itm card active">
						<h4 class="text-center tituloModalCheckout text-muted text-uppercase">Tarjeta de crédito o débito</h4>
					<hr>
								
								<div class="pymnt-cntnt">
										<div class="card-expl">
												<div class="credit"></div>
												<div class="debit"></div>
										</div>

										<div class="card-details">      
											
										<div class="sctn-row">                            
                            <div class="sctn-col">
                                <label>Monto</label><input type="text" class="form-control input-sm" autocomplete="off" name="monto" value="" disabled="true">
														</div>
														<div class="sctn-col l">
                                 <label>Descripción</label><input type="text" class="form-control input-sm" name="descripcion" value="" disabled="true">
                            </div>
							<div class="row">
              <div id="mostrar_correo_electronico" class="form-group col-md-12 col-sm-12 col-xs-12">
                <label for="card-holder">Correo Electr&oacute;nico *</label>
                <input id="card-holder" type="text" class="form-control input-sm correo_electronico" placeholder="email" autocomplete="off">
              </div>
			  </div>
                        </div> 
												<hr>
				
            <div class="row">
              <div class="form-group col-md-7 col-sm-7 col-xs-12">
                <label for="card-holder">Nombre del titular</label>
                <input id="card-holder" type="text" class="form-control input-sm" placeholder="Como aparece en la tarjeta" autocomplete="off" data-openpay-card="holder_name">
              </div>
              <div class="form-group col-md-5 col-sm-5 col-xs-12">
                <label for="">Fecha de expiraci&oacute;n</label>
                <div class="input-group expiration-date">
                  <input type="text" class="form-control input-sm soloNumeros" id="expiration_month" style="width:45%;float: left;" placeholder="MM" maxlength="2" data-openpay-card="expiration_month">
                  <span class="date-separator">/</span>
                  <input type="text" class="form-control input-sm soloNumeros" id="expiration_year" style="width:45%;float: right;" placeholder="AA"  maxlength="2" data-openpay-card="expiration_year">
                </div>
              </div>
              <div class="form-group col-sm-8">
                <label for="card-number">N&uacute;mero de tarjeta</label>
                <input id="card-number" type="text" class="form-control input-sm soloNumeros"  maxlength="16" autocomplete="off" data-openpay-card="card_number">
              </div>
              <div class="form-group col-sm-4">
                <label for="cvc">C&oacute;digo de seguridad</label>
                <input id="cvc" type="text" class="form-control input-sm soloNumeros" maxlength="3" placeholder="3 dígitos" data-openpay-card="cvv2">
							</div>
							<div class="checkBox">
					
					<label>
						
						<input id="PayPoliticas" type="checkbox">
					
							<small>
								He ledido y acepto condiciones de uso y políticas de privacidad&nbsp;
								<a href="https://www.yupashop.com/tienda/terminos-y-condiciones.html" target="_blank">Ver terminos y condiciones</a>
								&nbsp;&nbsp;
								<a href="https://www.yupashop.com/tienda/aviso-de-privacidad.html" target="_blank">Ver aviso de privacidad</a>
							</small>

					</label>

				</div>
              <div class="form-group col-sm-12">
                <button type="button" id="pay-button" class="btn btn-block btn-lg btn-default">Pagar</button>
              </div>
            </div>
          </div>									                       
										
										
								</div>
						</div>
				</form>
	<div class="modal-footer">
		<center>
	<nav aria-label="...">
  <ul class="pagination pagination-lg justify-content-center">
    <li id="b1" class="page-item">
      <a class="page-link" href="#">1</a>
    </li>
    <li id="b2" class="page-item">
			<a class="page-link" href="#">2</a></li>		
			<li id="b3" class="page-item">
			<a class="page-link" href="#">3</a></li>
  </ul>
</nav>
			</center>
	</div>

	</div>

</div>


<!--=====================================
MARIPOSITA
======================================-->

<div class="modal fade modalFormulario" data-backdrop="static" data-keyboard="false" id="modalSpinner" role="dialog" style="z-index: 1600;">
<div class="modal-body" style="color:black;position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%) scale(0.740741) translateZ(0px); zoom: 1;"> 
    <center>
      <span id="spinner" class="fa fa-spinner fa-pulse fa-3x fa-fw">
    </center>
  </div>
</div>