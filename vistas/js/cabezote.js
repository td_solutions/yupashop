/*=============================================
CABEZOTE
=============================================*/

//$("#btnCategorias").click(function(){

if (window.matchMedia("(max-width:767px)").matches) {

    $("#btnCategorias").after($("#categorias").slideToggle("fast"))

} else {

    $("#cabezote").after($("#categorias").slideToggle("fast"))

}


$(".pixelCategorias").mouseover(function() {
    $("[id^=sub_cat]").slideUp();
    var nombre_temporal = '#sub_cat_' + $(this)[0].text;
    $(nombre_temporal).slideDown();
});

// $(".pixelCategorias").mouseout(function(){
// 	var nombre_temporal = '#sub_cat_' + $(this)[0].text;
// 	$(nombre_temporal).slideToggle("fast");
// });

$("#cabezote").click(function() {
    $("[id^=sub_cat]").slideUp();
});

$("#btn_comprar_como_invitado").click(function() {
    var rutaActual = location.href;
    localStorage.setItem("rutaActual", rutaActual);

    var objInvitado = new FormData();
    objInvitado.append("registroInvitado", "si");
    $.ajax({
        url: rutaOculta + "ajax/registroInvitado.ajax.php",
        method: "POST",
        data: objInvitado,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            localStorage.setItem("invitado", "si");
            localStorage.setItem("usuario", respuesta);
            swal({
                title: "Bienvenido",
                text: "Ingresó como invitado",
                type: "success",
                confirmButtonText: "¡Ok!"
            });
            window.location = localStorage.getItem("rutaActual");
            $("#modalCheckout").modal("show");
            
            evento_checkout();
        },
        error: function(respuesta) {
            swal({
                title: "Error",
                text: "Ingreso como invitado",
                type: "error",
                confirmButtonText: "¡Cancelar!"
            });
        }
    });
});