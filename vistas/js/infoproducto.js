/*=============================================
CARRUSEL
=============================================*/
// $("html, body").animate({ 
//     scrollTop: $('.productos').offset()
//   }, 500);


$(".flexslider").flexslider({

    animation: "slide",
    controlNav: true,
    animationLoop: false,
    slideshow: false,
    itemWidth: 100,
    itemMargin: 5

});

$(".flexslider ul li img").click(function() {

    var capturaIndice = $(this).attr("value");

    $(".infoproducto figure.visor img").hide();

    $("#lupa" + capturaIndice).show();
})
$("#masTexto").click(function(e) {
    e.preventDefault();
});
/*=============================================
EFECTO LUPA
=============================================*/
$(".infoproducto figure.visor img").mouseover(function(event) {

    var capturaImg = $(this).attr("src");

    $(".lupa img").attr("src", capturaImg);

    $(".lupa").fadeIn("fast");

    $(".lupa").css({

        "height": $(".visorImg").height() + "px",
        "background": "#eee",
        "width": "100%"

    })

})

$(".infoproducto figure.visor img").mouseout(function(event) {

    $(".lupa").fadeOut("fast");

})

$(".infoproducto figure.visor img").mousemove(function(event) {

    var posX = event.offsetX;
    var posY = event.offsetY;

    $(".lupa img").css({

        "margin-left": -posX + "px",
        "margin-top": -posY + "px"

    })

})

/*=============================================
CONTADOR DE VISTAS
=============================================*/

var contador = 0;

$(window).on("load", function() {

    var vistas = $("span.vistas").html();
    var precio = $("span.vistas").attr("tipo");

    contador = Number(vistas) + 1;

    $("span.vistas").html(contador);

    // EVALUAMOS EL PRECIO PARA DEFINIR CAMPO A ACTUALIZAR

    if (precio == 0) {

        var item = "vistasGratis";

    } else {

        var item = "vistas";

    }

    // EVALUAMOS LA RUTA PARA DEFINIR EL PRODUCTO A ACTUALIZAR

    var urlActual = location.pathname;
    var ruta = urlActual.split("/");

    var datos = new FormData();

    datos.append("valor", contador);
    datos.append("item", item);
    datos.append("ruta", ruta.pop());


    $.ajax({

        url: rutaOculta + "ajax/producto.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta) {}

    });

})

/*=============================================
ALTURA COMENTARIOS
=============================================*/

$(".comentarios").css({
    "height": $(".comentarios .alturaComentarios").height() + "px",
    "overflow": "hidden",
    "margin-bottom": "20px"
})

$("#verMas").click(function(e) {

    e.preventDefault();

    if ($("#verMas").html() == "Ver más") {

        $(".comentarios").css({ "overflow": "inherit" });

        $("#verMas").html("Ver menos");

    } else {

        $(".comentarios").css({
            "height": $(".comentarios .alturaComentarios").height() + "px",
            "overflow": "hidden",
            "margin-bottom": "20px"
        })

        $("#verMas").html("Ver más");
    }

})

/*=============================================
GUARDAR PREGUNTA
=============================================*/

$(".pregunta-usuario").change(function() {

    var pregunta = $(this).val();
    //var pregunta = $(this).val();

    $("#btn-publicar-pregunta").click(function() {


        var datos = new FormData();

        var idUsuario = localStorage.getItem("usuario");
        var urlActual = location.pathname;
        var ruta = urlActual.split("/");
    

        datos.append("preguntaUsuario", pregunta);
        datos.append("idUsuario", idUsuario);
        datos.append("ruta", ruta.pop());

        $.ajax({

            url: "ajax/preguntasRespuestas.ajax.php",
            method: "POST",
            data: datos,
            cache: false,
            contentType: false,
            processData: false,
            success: function(respuesta) {

                if (eval(respuesta) == "ok") {
                    var page_y = $( document ).scrollTop();
                    window.location = urlActual;
                    
                    //document.scrollTo(page_y);
                    $(".productos").scrollTop( page_y );
                    $(".pregunta-usuario").val('');
                    swal({
                        title: "Pregunta publicada",
                        text: "¡Gracias por preguntar en breve será atendida su duda!",
                        type: "success",
                        confirmButtonText: "¡Cerrar!"
                    });

                }

            }

        })

    })

})