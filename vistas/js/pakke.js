var endPointApi = "https://seller.pakke.mx/api/v1";

$("#btn_calcular_costo_envio").click(function() {

    if (!validarDatosPakke()) {
        swal({
            title: "Error",
            text: "Compete los datos del envío",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false
        })
        return;
    } else
        $('#modalSpinner').modal('toggle');





    $("#lista_paqueteria").html("");
    var ApiKeyPakke = localStorage.getItem("mApiKeyPakke");
    if (ApiKeyPakke != null) {
        testPakkeApi(ApiKeyPakke);
    }
});


function testPakkeApi(PakkeApiKey) {

    try {
        filtros = new Object();
        filtros.Length = $("#pakke_length").val();
        filtros.Width = $("#pakke_width").val();
        filtros.Height = $("#pakke_height").val();
        filtros.Weight = $("#pakke_weight").val();
        var ZipCodeTo = $("#pakke_zipCodeTo").val();
        var objDatos = JSON.stringify({ 'Parcel': filtros, 'ZipCodeFrom': '36670', 'ZipCodeTo': ZipCodeTo });
        $.ajax({
            url: endPointApi + "/Shipments/rates",
            type: "POST",
            data: objDatos,
            async: true,
            contentType: 'application/json',
            beforeSend: function(xhr) {
                /* Authorization header */
                xhr.setRequestHeader("Authorization", PakkeApiKey);
                xhr.setRequestHeader("X-Mobile", "false");
            },
            //Authorization:PakkeApiKey,
            dataType: "json",
            success: function(respuesta) {
                if (respuesta.Pakke.length <= 0) {
                    swal({
                        title: "Error",
                        text: "Ocurrio un error al calcular los parámetros del envío",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    });
                    return;
                }
                var objeto = JSON.parse(JSON.stringify(respuesta.Pakke));
                //$("#ResellerServiceConfigId").val(objeto.Reseller.ResellerServiceConfigId);
                objeto.forEach(funcionForEach);
                //$(".seleccionePais").html('<select class="form-control" id="seleccionarPais" required></select>');

                function funcionForEach(item, index) {
                    if (item.BestOption)
                        $("#lista_paqueteria").append('<a class="list-group-item list-group-item-action" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home" precio_envio ="' + item.TotalPrice + '" CourierCode="' + item.CourierCode + '" CourierName="' + item.CourierName + '" CourierServiceId="' + item.CourierServiceId + '" CourierServiceName="' + item.CourierServiceName + '" DeliveryDays="' + item.DeliveryDays + '" EstimatedDeliveryDate="' + item.EstimatedDeliveryDate + '"  value="' + item.CourierServiceId + '"><strong>' + item.CourierName + " " + item.CourierServiceName + ": " + item.TotalPrice + '</strong></a>')
                    else
                        $("#lista_paqueteria").append('<a class="list-group-item list-group-item-action" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home" precio_envio ="' + item.TotalPrice + '" CourierCode="' + item.CourierCode + '" CourierName="' + item.CourierName + '" CourierServiceId="' + item.CourierServiceId + '" CourierServiceName="' + item.CourierServiceName + '" DeliveryDays="' + item.DeliveryDays + '" EstimatedDeliveryDate="' + item.EstimatedDeliveryDate + '"  value="' + item.CourierServiceId + '">' + item.CourierName + " " + item.CourierServiceName + ": " + item.TotalPrice + '</a>')
                }
                $('#lista_paqueteria a').on('click', function(e) {
                    $('#lista_paqueteria a').removeClass('active');
                    e.preventDefault()
                    if ($(this).hasClass('active'))
                        $(this).removeClass('active')
                    else
                        $(this).addClass('active');

                    establecerValoresEnvio();


                    $("#b2").removeClass("disabled");
                    $(".calculoEnvio").hide();
                    $(".formaPagoyProductosaComprar").show();
                    $("#titulo_realizar_pago").html('REALIZAR PAGO 2/2');
                });
            },
            error: function(res) {
                swal({
                    title: "Error",
                    text: eval(res.responseJSON).error.message,
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                })
            },
            complete: function() {
                $('#modalSpinner').modal('toggle');
            }
        });
    } catch (e) {
        swal({
            title: "Technical Report",
            text: e,
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false
        })
    }
}

function establecerValoresEnvio() {
    var precioFinalEnvio = 0;
    var CourierCode = "";
    var CourierName = "";
    var CourierServiceId = "";
    var CourierServiceName = "";
    var DeliveryDays = "";
    var EstimatedDeliveryDate = "";
    var TasaMinimaEnvioGratis = 0;
    if (localStorage.getItem("mApiKeyPakke") == 'undefinied')
        return;

    TasaMinimaEnvioGratis = Number(localStorage.getItem("mTasaMinimaEnvioGratis"));

    precioFinalEnvio = $("#lista_paqueteria .active").attr('precio_envio');

    CourierCode = $("#lista_paqueteria .active").attr('CourierCode');
    CourierName = $("#lista_paqueteria .active").attr('CourierName');
    CourierServiceId = $("#lista_paqueteria .active").attr('CourierServiceId');
    CourierServiceName = $("#lista_paqueteria .active").attr('CourierServiceName');
    DeliveryDays = $("#lista_paqueteria .active").attr('DeliveryDays');
    EstimatedDeliveryDate = $("#lista_paqueteria .active").attr('DeliveryDays');

    $("#CourierCode").val(CourierCode);
    $("#CourierName").val(CourierName);
    $("#CourierServiceId").val(CourierServiceId);
    $("#CourierServiceName").val(CourierServiceName);
    $("#DeliveryDays").val(DeliveryDays);
    $("#EstimatedDeliveryDate").val(EstimatedDeliveryDate);
    $("#TotalPrice").val(precioFinalEnvio);

    if (precioFinalEnvio != 'undefined') {
        //var resultadoPeso =$("#envionacional").val();
        var Enviogratis = 0;
        var suma = Number($(".valorSubtotal").html());
        if (suma < TasaMinimaEnvioGratis) {

            $(".valorTotalEnvio").html(precioFinalEnvio);
            $(".valorTotalEnvio").attr("valor", precioFinalEnvio);


        } else {

            $(".valorTotalEnvio").html(Enviogratis);
            $(".valorTotalEnvio").attr("valor", Enviogratis);
        }

    }

    sumaTotalCompra();
}

function obtenerPakkeApiKey() {
    var ruta = rutaOculta + "ajax/pakke.ajax.php";
    var datos = new FormData();
    var resultado;
    datos.append("accion", "obtenerPakkeApiKey");
    $.ajax({
        url: ruta,
        method: "POST",
        async: false,
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta) {
            resultado = eval(respuesta)[0].pakke_api_key;
        }

    });
    return resultado;
}

function obtenerTasaMinimaEnvioGratis() {
    var ruta = rutaOculta + "ajax/pakke.ajax.php";
    var datos = new FormData();
    var resultado;
    datos.append("accion", "obtenerTasaMinimaEnvioGratis");
    $.ajax({
        url: ruta,
        method: "POST",
        async: false,
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta) {
            resultado = eval(respuesta)[0].envioInternacional;
        }

    });
    return resultado;
}

/*=============================================
VALIDAR DATOS PAKKE
/*=============================================*/
function validarDatosPakke() {
    var respuesta = true;
    if ($("#pakke_length").val().trim() == "")
        respuesta = false;
    if ($("#pakke_width").val().trim() == "")
        respuesta = false;
    if ($("#pakke_height").val().trim() == "")
        respuesta = false;
    if ($("#pakke_weight").val().trim() == "")
        respuesta = false;
    if ($("#pakke_adress").val().trim() == "")
        respuesta = false;
    if ($("#pakke_zipCodeTo").val().trim() == "")
        respuesta = false;
    if ($("#pakke_colonia").val().trim() == "")
        respuesta = false;
    if ($("#pakke_ciudad").val().trim() == "")
        respuesta = false;
    if ($("#pakke_estado").val().trim() == "")
        respuesta = false;

    return respuesta;
}