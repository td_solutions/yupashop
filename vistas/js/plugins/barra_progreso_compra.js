var endPointApi = "https://seller.pakke.mx/api/v1";

$(document).ready(function () {    
    var mApiKeyPakke = obtenerPakkeApiKey();
    localStorage.setItem("mApiKeyPakke", mApiKeyPakke);
    $('.btn_detalles').click(function(){
        var No_Guia;
        $('#h5_hora_entrega').css('display','none'); 
        $('#h5_recibido_por').css('display','none'); 
        No_Guia = $(this).attr('no_guia');
        var CourierCode = $(this).attr('courier_code');
        if(No_Guia=='undefinied' || No_Guia.length <= 0)
        return;
        Consultar_Detalles_Pakke(No_Guia,CourierCode);
    });
});

function obtenerPakkeApiKey() {
    var ruta = rutaOculta + "ajax/pakke.ajax.php";
    var datos = new FormData();
    var resultado;
    datos.append("accion", "obtenerPakkeApiKey");
    $.ajax({
        url: ruta,
        method: "POST",
        async: false,
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta) {
            resultado = eval(respuesta)[0].pakke_api_key;
            $("#pakkeApiKey").val(resultado);
        }

    });
    return resultado;
}
function Consultar_Detalles_Pakke(p_guia,p_courier_code){    
    $("#txt_no_envio").html(p_guia);
    $("#historia").html("");
    try {           
        mApiKeyPakke = localStorage.getItem("mApiKeyPakke");
        if(mApiKeyPakke != null){
    $.ajax({
        url:endPointApi+"/Shipments/tracking?courierCode="+p_courier_code+"&trackingNumber="+p_guia,
        type: "GET",
        async:false,
        contentType: 'application/json',
        beforeSend: function (xhr) {
            /* Authorization header */
            xhr.setRequestHeader("Authorization", mApiKeyPakke );
        },  
        //Authorization:PakkeApiKey,
        dataType:"json",
        success:function(respuesta){
            
            var objeto = JSON.parse(JSON.stringify(respuesta));
            $('#txt_paqueteria').html(objeto[0].CourierServiceId);//CourierServiceId
            $('#txt_estatus').html(objeto[0].TrackingStatus);
            if(objeto[0].TrackingStatus=='DELIVERED'){
                $('#txt_hora_entrega').html(formato_fecha(objeto[0].ReceivedAt));
                $('#txt_recibido_por').html(objeto[0].ReceivedBy);
                $('#h5_hora_entrega').css('display','block'); 
                $('#h5_recibido_por').css('display','block'); 
            }

            var obj_historia = JSON.parse(JSON.stringify(objeto[0].History));
            obj_historia.forEach(funcionForEach);
            function funcionForEach(item, index){
                $("#historia").append("<strong>"+ formato_fecha(item.Date) +"</strong>  "+item.Details + "<br>");
            }            
        },
    error:function(res){
        swal({
            title: "Error",
            text: eval(res.responseJSON).error.message,
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false
          })
    },
    complete:function(){
        $('#modalSpinner').modal('toggle');
    }
    });  
}
    }
catch (e) {
    swal({
        title: "Technical Report",
        text: e,
        type: "warning",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false
      })
}  
}

function formato_fecha(fecha) {
    var monthNames = [
      "Enero", "Februaro", "Marzo",
      "Abril", "Mayo", "Junio", "Julio",
      "Agosto", "Septiembre", "Octubre",
      "Noviembre", "Diciembre"
    ];
    var dayNames = [
        "Lunes", "Martes", "Miercoles",
        "Jueves", "Viernes", "Sabado", "Domingo"
      ];
    var dt_fecha = new Date(fecha);
    var day = dt_fecha.getDay();
    var dia = dt_fecha.getDate()
    var monthIndex = dt_fecha.getMonth();
    var year = dt_fecha.getFullYear();
    var hora = dt_fecha.getHours();
    var minutos = dt_fecha.getMinutes();
    var segundos = dt_fecha.getSeconds();
  
    return dayNames[day] + ', ' + dia + ' de ' + monthNames[monthIndex] + ' ' + year + ' ' + hora + ':' + minutos;
  }