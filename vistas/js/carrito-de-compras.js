$(".btnPagar").hide();
$(".formPayu").show();
$("#b2").addClass("disabled");
$("#b3").addClass("disabled");
var mDescuentoPrimeraCompra = "N";
/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
VISUALIZAR LA CESTA DEL CARRITO DE COMPRAS
=============================================*/

if (localStorage.getItem("cantidadCesta") !== null) {

    $(".cantidadCesta").html(localStorage.getItem("cantidadCesta"));
    $(".sumaCesta").html(localStorage.getItem("sumaCesta"));

} else {

    $(".cantidadCesta").html("0");
    $(".sumaCesta").html("0");


}



function Consultar_Descuento_Primera_Compra(){

    var datosDescuento = new FormData();
    datosDescuento.append("descuento", "si");

    $.ajax({
        url: rutaOculta + "ajax/descuento_primera_compra.ajax.php",
        method: "POST",
        data: datosDescuento,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {
            if(respuesta["descuento_primera_compra"]=="S"){
                $("#tr_descuento_primera_compra").show();
                localStorage.setItem("descuento_primera_compra", "si");
            }
            else
                localStorage.setItem("descuento_primera_compra", "no");
        }
    });
}

/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
VISUALIZAR LOS PRODUCTOS EN LA PÁGINA CARRITO DE COMPRAS
=============================================*/

if (localStorage.getItem("listaProductos") !== null) {

    var listaCarrito = JSON.parse(localStorage.getItem("listaProductos"));

    listaCarrito.forEach(funcionForEach);

    function funcionForEach(item, index) {

        var datosProducto = new FormData();
        var precio = 0;

        datosProducto.append("id", item.idProducto);

        $.ajax({

            url: rutaOculta + "ajax/producto.ajax.php",
            method: "POST",
            data: datosProducto,
            cache: false,
            contentType: false,
            processData: false,
            dataType: "json",
            success: function(respuesta) {

                if (respuesta["precioOferta"] == 0) {

                    precio = respuesta["precio"];

                } else {

                    precio = respuesta["precioOferta"];
                }

                $(".cuerpoCarrito").append(

                    '<div clas="row itemCarrito">' +

                    '<div class="col-sm-1 col-xs-12">' +

                    '<br>' +

                    '<center>' +

                    '<button class="btn btn-default backColor quitarItemCarrito" idProducto="' + item.idProducto + '" talla="' + item.talla + '" color="' + item.color + '" peso="' + item.peso + '" longitud="' + item.longitud + '" ancho="' + item.ancho + '" alto="' + item.alto + '" stock="' + item.stock + '" sku="' + item.sku + '">' +

                    '<i class="fa fa-times"></i>' +

                    '</button>' +

                    '</center>' +

                    '</div>' +
                    '<div class="col-sm-1 col-xs-12">' +

                    '<figure>' +

                    '<img src="' + item.imagen + '" class="img-thumbnail">' +

                    '</figure>' +

                    '</div>' +

                    '<div class="col-sm-4 col-xs-12">' +

                    '<br>' +

                    '<p class="tituloCarritoCompra text-left">' + item.titulo + '</p>' +

                    '</div>' +

                    '<div class="col-md-2 col-sm-1 col-xs-12">' +

                    '<br>' +

                    '<p class="precioCarritoCompra text-center">MXN $<span>' + precio + '</span></p>' +

                    '</div>' +

                    '<div class="col-md-2 col-sm-3 col-xs-8">' +

                    '<br>' +

                    '<div class="col-xs-8">' +

                    '<center>' +

                    '<input type="number" class="form-control cantidadItem" min="1" value="' + item.cantidad + '" tipo="' + item.tipo + '" precio="' + item.precio + '" idProducto="' + item.idProducto + + '" sku="' + item.sku + '" item="' + index + '">' +

                    '</center>' +

                    '</div>' +

                    '</div>' +

                    '<div class="col-md-2 col-sm-1 col-xs-4 text-center">' +

                    '<br>' +

                    '<p class="subTotal' + index + ' subtotales">' +

                    '<strong>MXN $<span>' + item.precio + '</span></strong>' +

                    '</p>' +

                    '</div>' +

                    '</div>' +

                    '<div class="clearfix"></div>' +

                    '<hr>');

                /*=============================================
                EVITAR MANIPULAR LA CANTIDAD EN PRODUCTOS VIRTUALES
                =============================================*/

                $(".cantidadItem[tipo='virtual']").attr("readonly", "true");

                /*=============================================
                /*=============================================
                /*=============================================
                /*=============================================
                /*=============================================
                ACTUALIZAR SUBTOTAL
                =============================================*/
                var precioCarritoCompra = $(".cuerpoCarrito .precioCarritoCompra span");
                var cantidadItem = $(".cuerpoCarrito .cantidadItem");

                for (var i = 0; i < precioCarritoCompra.length; i++) {

                    var precioCarritoCompraArray = $(precioCarritoCompra[i]).html();
                    var cantidadItemArray = $(cantidadItem[i]).val();
                    var idProductoArray = $(cantidadItem[i]).attr("idProducto");

                    $(".subTotal" + i).html('<strong>MXN $<span>' + (precioCarritoCompraArray * cantidadItemArray) + '</span></strong>')

                    sumaSubtotales();
                    cestaCarrito(precioCarritoCompra.length);

                }

            }

        })

        /*=============================================
        /*=============================================
        /*=============================================
        /*=============================================
        /*=============================================
        ACTUALIZAR SUBTOTAL
        =============================================*/
        var precioCarritoCompra = $(".cuerpoCarrito .precioCarritoCompra span");
        var cantidadItem = $(".cuerpoCarrito .cantidadItem");

        for (var i = 0; i < precioCarritoCompra.length; i++) {

            var precioCarritoCompraArray = $(precioCarritoCompra[i]).html();
            var cantidadItemArray = $(cantidadItem[i]).val();
            var idProductoArray = $(cantidadItem[i]).attr("idProducto");

            $(".subTotal" + i).html('<strong>MXN $<span>' + (precioCarritoCompraArray * cantidadItemArray) + '</span></strong>')

            sumaSubtotales();
            cestaCarrito(precioCarritoCompra.length);

        }

    }

} else {

    $(".cuerpoCarrito").html('<div class="well">Aún no hay productos en el carrito de compras.</div>');
    $(".sumaCarrito").hide();
    $(".cabeceraCheckout").hide();
}

/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
AGREGAR AL CARRITO
=============================================*/

$(".agregarCarrito").click(function() {

    var idProducto = $(this).attr("idProducto");
    var imagen = $(this).attr("imagen");
    var titulo = $(this).attr("titulo");
    var titulo_detalle = "";
    var precio = $(this).attr("precio");
    var tipo = $(this).attr("tipo");
    var peso = $(this).attr("peso");
    var ancho = $(this).attr("ancho");
    var alto = $(this).attr("alto");
    var longitud = $(this).attr("longitud");
    var stock = $(this).attr("stock");
    var sku = $(this).attr("sku");
    var talla = $("#seleccionarTalla :selected").val();
    var color = $("#seleccionarColor :selected").val();
    var agregarAlCarrito = false;
    var idUsuario = localStorage.getItem("usuario");
    /*=============================================
    VALIDAR STOCK
    =============================================*/
    if (stock <= 0) {
        swal({
            title: "Este producto no se encuentra en existencia",
            text: "",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "¡Volver!",
            closeOnConfirm: false
        });
        return;
    }
    /*=============================================
    CAPTURAR DETALLES
    =============================================*/

    if (tipo == "virtual") {

        agregarAlCarrito = true;

    } else {

        var seleccionarDetalle = $(".seleccionarDetalle");

        for (var i = 0; i < seleccionarDetalle.length; i++) {

            if ($(seleccionarDetalle[i]).val() == "") {

                swal({
                    title: "Debe seleccionar Talla y Color",
                    text: "",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "¡Seleccionar!",
                    closeOnConfirm: false
                })

                return;

            } else {

                titulo_detalle = titulo_detalle + "-" + $(seleccionarDetalle[i]).val();

                agregarAlCarrito = true;

            }

        }

    }

    /*=============================================
    ALMACENAR EN EL LOCALSTARGE LOS PRODUCTOS AGREGADOS AL CARRITO
    =============================================*/

    if (agregarAlCarrito) {

        /*=============================================
        RECUPERAR ALMACENAMIENTO DEL LOCALSTORAGE
        =============================================*/

        if (localStorage.getItem("listaProductos") == null) {

            listaCarrito = [];

        } else {

            var listaProductos = JSON.parse(localStorage.getItem("listaProductos"));

            for (var i = 0; i < listaProductos.length; i++) {

                if (listaProductos[i]["idProducto"] == idProducto && listaProductos[i]["tipo"] == "virtual") {

                    swal({
                        title: "El producto ya está agregado al carrito de compras",
                        text: "",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "¡Volver!",
                        closeOnConfirm: false
                    })

                    return;

                }

            }

            listaCarrito.concat(localStorage.getItem("listaProductos"));

        }

        listaCarrito.push({
            "idProducto": idProducto,
            "imagen": imagen,
            "titulo": titulo,
            "precio": precio,
            "tipo": tipo,
            "peso": peso,
            "longitud": longitud,
            "ancho": ancho,
            "alto": alto,
            "stock": stock,
            "sku": sku,
            "cantidad": "1",
            "talla": talla,
            "color":color
        });

        localStorage.setItem("listaProductos", JSON.stringify(listaCarrito));
        var objElementoCarrito = new FormData();
        objElementoCarrito.append("agregarCarritoOlvidado", "si");
        objElementoCarrito.append("idUsuario", idUsuario);
        objElementoCarrito.append("idProducto", idProducto);
        objElementoCarrito.append("cantidad", 1);
        //Agregar al carrito olvidado
        $.ajax({

                url: rutaOculta + "ajax/carritoOlvidado.ajax.php",
                method: "POST",
                data: objElementoCarrito,
                cache: false,
                contentType: false,
                processData: false,
                dataType: "json",
                success: function(respuesta) {

                }

            })
            //listaCarrito

        /*=============================================
        ACTUALIZAR LA CESTA
        =============================================*/

        var cantidadCesta = Number($(".cantidadCesta").html()) + 1;
        var sumaCesta = Number($(".sumaCesta").html()) + Number(precio);

        $(".cantidadCesta").html(cantidadCesta);
        $(".sumaCesta").html(sumaCesta);

        localStorage.setItem("cantidadCesta", cantidadCesta);
        localStorage.setItem("sumaCesta", sumaCesta);

        /*=============================================
        MOSTRAR ALERTA DE QUE EL PRODUCTO YA FUE AGREGADO
        =============================================*/

        swal({
                title: "",
                text: "¡Se ha agregado un nuevo producto al carrito de compras!",
                type: "success",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                cancelButtonText: "¡Continuar comprando!",
                confirmButtonText: "¡Ir a mi carrito de compras!",
                closeOnConfirm: false
            },
            function(isConfirm) {
                if (isConfirm) {
                    window.location = rutaOculta + "carrito-de-compras";
                }
            });

    }

})

/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
QUITAR PRODUCTOS DEL CARRITO
=============================================*/

$(document).on("click", ".quitarItemCarrito", function() {

    $(this).parent().parent().parent().remove();

    var idProducto = $(".cuerpoCarrito button");
    var imagen = $(".cuerpoCarrito img");
    var titulo = $(".cuerpoCarrito .tituloCarritoCompra");
    var precio = $(".cuerpoCarrito .precioCarritoCompra span");
    var cantidad = $(".cuerpoCarrito .cantidadItem");

    /*=============================================
    SI AÚN QUEDAN PRODUCTOS VOLVERLOS AGREGAR AL CARRITO (LOCALSTORAGE)
    =============================================*/

    listaCarrito = [];

    if (idProducto.length != 0) {

        for (var i = 0; i < idProducto.length; i++) {

            var idProductoArray = $(idProducto[i]).attr("idProducto");
            var imagenArray = $(imagen[i]).attr("src");
            var tituloArray = $(titulo[i]).html();
            var precioArray = $(precio[i]).html();
            var pesoArray = $(idProducto[i]).attr("peso");
            var longArray = $(idProducto[i]).attr("longitud");
            var altoArray = $(idProducto[i]).attr("alto");
            var anchoArray = $(idProducto[i]).attr("ancho");
            var tallaArray = $(idProducto[i]).attr("talla");
            var colorArray = $(idProducto[i]).attr("color");
            var tipoArray = $(cantidad[i]).attr("tipo");
            var stockArray = $(cantidad[i]).attr("stock");
            var cantidadArray = $(cantidad[i]).val();

            listaCarrito.push({
                "idProducto": idProductoArray,
                "imagen": imagenArray,
                "titulo": tituloArray,
                "precio": precioArray,
                "tipo": tipoArray,
                "peso": pesoArray,
                "longitud": longArray,
                "ancho": anchoArray,
                "alto": altoArray,
                "stock": stockArray,
                "cantidad": cantidadArray,
                "talla": tallaArray,
                "color": colorArray
            });

        }

        localStorage.setItem("listaProductos", JSON.stringify(listaCarrito));

        sumaSubtotales();
        cestaCarrito(listaCarrito.length);


    } else {

        /*=============================================
        SI YA NO QUEDAN PRODUCTOS HAY QUE REMOVER TODO
        =============================================*/

        localStorage.removeItem("listaProductos");

        localStorage.setItem("cantidadCesta", "0");

        localStorage.setItem("sumaCesta", "0");

        $(".cantidadCesta").html("0");
        $(".sumaCesta").html("0");

        $(".cuerpoCarrito").html('<div class="well">Aún no hay productos en el carrito de compras.</div>');
        $(".sumaCarrito").hide();
        $(".cabeceraCheckout").hide();

    }

    var current_product_id = $(this).attr('idproducto');
    var idUsuario = localStorage.getItem("usuario");
    var objElementoCarrito = new FormData();
    objElementoCarrito.append("quitarCarritoOlvidado", "si");
    objElementoCarrito.append("idUsuario", idUsuario);
    objElementoCarrito.append("idProducto", current_product_id);
    //Agregar al carrito olvidado
    $.ajax({

        url: rutaOculta + "ajax/carritoOlvidado.ajax.php",
        method: "POST",
        data: objElementoCarrito,
        cache: false,
        contentType: false,
        processData: false,
        dataType: "json",
        success: function(respuesta) {

        }

    })
})


/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
GENERAR SUBTOTAL DESPUES DE CAMBIAR CANTIDAD
=============================================*/
$(document).on("change", ".cantidadItem", function() {

    var cantidad = $(this).val();
    var precio = $(this).attr("precio");
    var idProducto = $(this).attr("idProducto");
    var item = $(this).attr("item");

    $(".subTotal" + item).html('<strong>MXN $<span>' + (cantidad * precio) + '</span></strong>');

    /*=============================================
    ACTUALIZAR LA CANTIDAD EN EL LOCALSTORAGE
    =============================================*/

    var idProducto = $(".cuerpoCarrito button");
    var imagen = $(".cuerpoCarrito img");
    var titulo = $(".cuerpoCarrito .tituloCarritoCompra");
    var precio = $(".cuerpoCarrito .precioCarritoCompra span");
    var cantidad = $(".cuerpoCarrito .cantidadItem");
    listaCarrito = [];

    for (var i = 0; i < idProducto.length; i++) {

        var idProductoArray = $(idProducto[i]).attr("idProducto");
        var imagenArray = $(imagen[i]).attr("src");
        var tituloArray = $(titulo[i]).html();
        var precioArray = $(precio[i]).html();
        var pesoArray = $(idProducto[i]).attr("peso");
        var longArray = $(idProducto[i]).attr("longitud");
        var anchoArray = $(idProducto[i]).attr("ancho");
        var altoArray = $(idProducto[i]).attr("alto");
        var stockArray = $(idProducto[i]).attr("stock");
        var tipoArray = $(cantidad[i]).attr("tipo");
        var tallaArray = $(idProducto[i]).attr("talla");
        var colorArray = $(idProducto[i]).attr("color");
        var cantidadArray = $(cantidad[i]).val();

        if (cantidadArray > stockArray) {
            swal({
                title: "Este producto solo tiene " + stockArray + " en existencia",
                text: "",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "¡Volver!",
                closeOnConfirm: false
            });
            $(this).val(stockArray);
            return;
        }

        listaCarrito.push({
            "idProducto": idProductoArray,
            "imagen": imagenArray,
            "titulo": tituloArray,
            "precio": precioArray,
            "tipo": tipoArray,
            "peso": pesoArray,
            "longitud": longArray,
            "ancho": anchoArray,
            "alto": altoArray,
            "stock": stockArray,
            "cantidad": cantidadArray,
            "talla": tallaArray,
            "color": colorArray
        });

    }

    localStorage.setItem("listaProductos", JSON.stringify(listaCarrito));

    sumaSubtotales();
    cestaCarrito(listaCarrito.length);
})

/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
SUMA DE TODOS LOS SUBTOTALES
=============================================*/
function sumaSubtotales() {
var descuento_primera_vez = 'no';
descuento_primera_vez = localStorage.getItem("descuento_primera_compra");
    var subtotales = $(".subtotales span");
    var arraySumaSubtotales = [];

    for (var i = 0; i < subtotales.length; i++) {

        var subtotalesArray = $(subtotales[i]).html();
        arraySumaSubtotales.push(Number(subtotalesArray));

    }


    function sumaArraySubtotales(total, numero) {

        return total + numero;

    }

    var sumaTotal = arraySumaSubtotales.reduce(sumaArraySubtotales);
    var Totaldescuento = sumaTotal * 0.1;

    $(".sumaSubTotal").html('<strong>MXN $<span>' + (sumaTotal).toFixed(2) + '</span></strong>');
    if(descuento_primera_vez == "si")
    {
        $(".valorDescuentoPrimeraCompra").html((Totaldescuento).toFixed(2));
    }
    $(".sumaCesta").html((sumaTotal).toFixed(2));

    localStorage.setItem("sumaCesta", (sumaTotal).toFixed(2));


}

/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
ACTUALIZAR CESTA AL CAMBIAR CANTIDAD
=============================================*/
function cestaCarrito(cantidadProductos) {

    /*=============================================
    SI HAY PRODUCTOS EN EL CARRITO
    =============================================*/

    if (cantidadProductos != 0) {

        var cantidadItem = $(".cuerpoCarrito .cantidadItem");

        var arraySumaCantidades = [];

        for (var i = 0; i < cantidadItem.length; i++) {

            var cantidadItemArray = $(cantidadItem[i]).val();
            arraySumaCantidades.push(Number(cantidadItemArray));

        }

        function sumaArrayCantidades(total, numero) {

            return total + numero;

        }

        var sumaTotalCantidades = arraySumaCantidades.reduce(sumaArrayCantidades);

        $(".cantidadCesta").html(sumaTotalCantidades);
        localStorage.setItem("cantidadCesta", sumaTotalCantidades);

    }

}

function evento_checkout() {

    $(".listaProductos table.tablaProductos tbody").html("");

    var idUsuario = $(this).attr("idUsuario");
    var peso = $(".cuerpoCarrito button, .comprarAhora button");
    var titulo = $(".cuerpoCarrito .tituloCarritoCompra, .comprarAhora .tituloCarritoCompra");
    var cantidad = $(".cuerpoCarrito .cantidadItem, .comprarAhora .cantidadItem");
    var subtotal = $(".cuerpoCarrito .subtotales span, .comprarAhora .subtotales span");
    var tipoArray = [];
    var cantidadColor = [];
    var cantidadTalla = [];
    var cantidadPeso = [];
    var cantidadLongitud = [];
    var cantidadAlto = [];
    var cantidadAncho = [];

    $(".calculoEnvio").show();
    $(".formaPagoyProductosaComprar").hide();
    $("#payment-form").hide();
    $("#titulo_realizar_pago").html('REALIZAR PAGO 1/3');


    Consultar_Descuento_Primera_Compra();
    /*=============================================
    SUMA SUBTOTAL
    =============================================*/

    var sumaSubTotal = $(".sumaSubTotal span")

    $(".valorSubtotal").html($(sumaSubTotal).html());
    $(".valorSubtotal").attr("valor", $(sumaSubTotal).html());

    /*=============================================
    TASAS DE IMPUESTO
    =============================================*/

    var impuestoTotal = ($(".valorSubtotal").html() * $("#tasaImpuesto").val()) / 100;

    $(".valorTotalImpuesto").html((impuestoTotal).toFixed(2));
    $(".valorTotalImpuesto").attr("valor", (impuestoTotal).toFixed(2));

    sumaTotalCompra()

    /*=============================================
    VARIABLES ARRAY 
    =============================================*/

    for (var i = 0; i < titulo.length; i++) {

        var pesoArray = $(peso[i]).attr("peso");
        var longArray = $(peso[i]).attr("longitud");
        var altoArray = $(peso[i]).attr("alto");
        var anchoArray = $(peso[i]).attr("ancho");
        var tallaArray = $(peso[i]).attr("talla");
        var colorArray = $(peso[i]).attr("color");
        var skuArray = $(peso[i]).attr("sku");
        var tituloArray = $(titulo[i]).html();
        var cantidadArray = $(cantidad[i]).val();
        var subtotalArray = $(subtotal[i]).html();
        /*=============================================
        EVALUAR EL PESO DE ACUERDO A LA CANTIDAD DE PRODUCTOS
        =============================================*/

        cantidadPeso[i] = (pesoArray * cantidadArray);
        cantidadLongitud[i] = (longArray * 1) / 100;
        cantidadAlto[i] = (altoArray * cantidadArray) / 100;
        cantidadAncho[i] = (anchoArray * cantidadArray) / 100;

        function sumaArrayPeso(total, numero) {
            return total + numero;
        }

        var sumaTotalPeso = cantidadPeso.reduce(sumaArrayPeso);
        var sumaTotalLongitud = cantidadLongitud.reduce(sumaArrayPeso);
        var sumaTotalAncho = cantidadAncho.reduce(sumaArrayPeso);
        var sumaTotalAlto = cantidadAlto.reduce(sumaArrayPeso);

        /*=============================================
        MOSTRAR PRODUCTOS DEFINITIVOS A COMPRAR
        =============================================*/

        $(".listaProductos table.tablaProductos tbody").append('<tr>' +
            '<td class="valorTitulo">' + tituloArray + '</td>' +
            '<td class="valorCantidad">' + cantidadArray + '</td>' +
            '<td class="valorTalla">' + tallaArray + '</td>' +
            '<td class="valorColor">' + colorArray + '</td>' +
            '<td class="valorSku">' + skuArray + '</td>' +
            '<td>$<span class="valorItem" valor="' + subtotalArray + '">' + subtotalArray + '</span></td>' +
            '<tr>');

        /*=============================================
        SELECCIONAR ENVÍO SI HAY PRODUCTOS FÍSICOS
        =============================================*/

        tipoArray.push($(cantidad[i]).attr("tipo"));

        function checkTipo(tipo) {

            return tipo == "fisico";

        }

    }
    $("#pakke_length").val(sumaTotalLongitud);
    $("#pakke_weight").val(sumaTotalPeso);
    $("#pakke_width").val(sumaTotalAncho);
    $("#pakke_height").val(sumaTotalAlto);
    /*=============================================
    EXISTEN PRODUCTOS FÍSICOS
    =============================================*/

    if (tipoArray.find(checkTipo) == "fisico") {
        //$(".formaPago").
        var mApiKeyPakke = obtenerPakkeApiKey();
        localStorage.setItem("mApiKeyPakke", mApiKeyPakke);

        var mTasaMinimaEnvioGratis = obtenerTasaMinimaEnvioGratis();
        localStorage.setItem("mTasaMinimaEnvioGratis", mTasaMinimaEnvioGratis);

        $(".seleccionePais").html('<select class="form-control" id="seleccionarPais" required>' +

            '<option value="">Seleccione un estado</option>' +

            '</select>');


        $(".calculoEnvio").show();

        $(".btnPagar").attr("tipo", "fisico");

        /* $.ajax({
        	url:rutaOculta+"vistas/js/plugins/countries.json",
        	type: "GET",
        	cache: false,
        	contentType: false,
        	processData:false,
        	dataType:"json",
        	success: function(respuesta){

        		respuesta.forEach(seleccionarPais);

        		function seleccionarPais(item, index){

        			var pais = item.name;
        			var codPais = item.code;

        			$("#seleccionarPais").append('<option value="'+codPais+'">'+pais+'</option>');
        		
        		}

        	}
        }) */

        /*=============================================
        EVALUAR TASAS DE ENVÍO SI EL PRODUCTO ES FÍSICO
        =============================================*/

        $("#seleccionarPais").change(function() {

            $(".alert").remove();

            var pais = $(this).val();
            var tasaPais = $("#tasaPais").val();

            if (pais == tasaPais) {

                var resultadoPeso = $("#envionacional").val();
                var Enviogratis = 0;

                var suma = $(".sumaSubTotal span");
                if (resultadoPeso > suma) {

                    $(".valorTotalEnvio").html(resultadoPeso);
                    $(".valorTotalEnvio").attr("valor", resultadoPeso);


                } else {

                    $(".valorTotalEnvio").html(Enviogratis);
                    $(".valorTotalEnvio").attr("valor", Enviogratis);
                }

            } else {

                var resultadoPeso = $("#envioInternacional").val();
                var Enviogratis = 55;

                var suma = Number($(".valorSubtotal").html());
                if (suma > resultadoPeso) {


                    $(".valorTotalEnvio").html(Enviogratis);
                    $(".valorTotalEnvio").attr("valor", Enviogratis);



                } else {
                    $(".valorTotalEnvio").html(resultadoPeso);
                    $(".valorTotalEnvio").attr("valor", resultadoPeso);
                }

            }

            sumaTotalCompra();
            pagarConPayu();

        })

    } else {
        $(".formaPagoyProductosaComprar").show();
        $(".btnPagar").attr("tipo", "virtual");
    }
}
/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
CHECKOUT
=============================================*/

$("#btnCheckout").click(function() {
    evento_checkout();
});




/*=============================================
		NAVEGACION DE ENVIO/FORMA DE PAGO
		=============================================*/
$("#b1").click(function() {
    $(".calculoEnvio").show();
    $(".formaPagoyProductosaComprar").hide();
    $("#payment-form").hide();
    $("#titulo_realizar_pago").html('REALIZAR PAGO 1/3');
    //$("#b2").addClass("disabled");
});

$("#b2").click(function() {
    if ($(this).hasClass('disabled'))
        return;
    $(".calculoEnvio").hide();
    $("#payment-form").hide();
    $(".formaPagoyProductosaComprar").show();
    $("#titulo_realizar_pago").html('REALIZAR PAGO 2/3');
});



/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
SUMA TOTAL DE LA COMPRA
=============================================*/
function sumaTotalCompra() {
    var descuento_primera_vez = 'no';
    descuento_primera_vez = localStorage.getItem("descuento_primera_compra");
    var descuentos = 0;

    var sumaTotalTasas = Number($(".valorSubtotal").html()) +
        Number($(".valorTotalEnvio").html()) +
        Number($(".valorTotalImpuesto").html());

        if(descuento_primera_vez == 'si'){
            descuentos = Number($(".valorDescuentoPrimeraCompra").html());
            if(descuentos<=0){
                var subtotal_Temp = Number($(".valorSubtotal").html());
                descuentos = parseFloat(subtotal_Temp * 0.1).toFixed(2);
                $(".valorDescuentoPrimeraCompra").html(descuentos);
            }
            sumaTotalTasas = sumaTotalTasas - descuentos;
        }

    $(".valorTotalCompra").html((sumaTotalTasas).toFixed(2));
    $(".valorTotalCompra").attr("valor", (sumaTotalTasas).toFixed(2));

    localStorage.setItem("total", hex_md5($(".valorTotalCompra").html()));


}

/*=============================================
/*=============================================
/*=============================================
/*=============================================
MÉTODO DE PAGO PARA CAMBIO DE DIVISA
=============================================*/

/* var metodoPago = "paypal";
divisas(metodoPago);

$("input[name='pago']").change(function(){

	var metodoPago = $(this).val();

	divisas(metodoPago);

	if(metodoPago == "payu"){

		$(".btnPagar").hide();			
		$(".formPayu").show();

		//pagarConOpenPay();

	}else{

		$(".btnPagar").show();
		$(".formPayu").hide();

	}

})
 */


/*=============================================
BOTÓN PAGAR OPENPAY
=============================================*/
$(".btnPagarOpenPay").click(function() {

    var titulo = $(".valorTitulo");
    var total = $(".valorTotalCompra").html();

    var tituloArray = [];

    for (var i = 0; i < titulo.length; i++) {

        tituloArray[i] = $(titulo[i]).html();

    }

    var description = tituloArray.toString();
    $(".calculoEnvio").hide();
    $(".formaPagoyProductosaComprar").hide();
    $("#payment-form").show();
    $("[name='monto']").val(total);
    $("[name='descripcion']").val(description);
    $("#amount").val(total);
    $("#amount").val(total);
    $("#description").val(description);

    $("#titulo_realizar_pago").html('REALIZAR PAGO 1/3');
    //$("#b2").addClass("disabled");
});


/*=============================================
/*=============================================
/*=============================================
/*=============================================
/*=============================================
AGREGAR PRODUCTOS GRATIS
=============================================*/
$(".agregarGratis").click(function() {

    var idProducto = $(this).attr("idProducto");
    var idUsuario = $(this).attr("idUsuario");
    var tipo = $(this).attr("tipo");
    var titulo = $(this).attr("titulo");
    var titulo_detalle = "";
    var agregarGratis = false;

    /*=============================================
    VERIFICAR QUE NO TENGA EL PRODUCTO ADQUIRIDO
    =============================================*/

    var datos = new FormData();

    datos.append("idUsuario", idUsuario);
    datos.append("idProducto", idProducto);

    $.ajax({
        url: rutaOculta + "ajax/carrito.ajax.php",
        method: "POST",
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta) {

            if (respuesta != "false") {

                swal({
                    title: "¡Usted ya adquirió este producto!",
                    text: "",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Regresar",
                    closeOnConfirm: false
                })


            } else {

                if (tipo == "virtual") {

                    agregarGratis = true;

                } else {

                    var seleccionarDetalle = $(".seleccionarDetalle");

                    for (var i = 0; i < seleccionarDetalle.length; i++) {

                        if ($(seleccionarDetalle[i]).val() == "") {

                            swal({
                                title: "Debe seleccionar Talla y Color",
                                text: "",
                                type: "warning",
                                showCancelButton: false,
                                confirmButtonColor: "#DD6B55",
                                confirmButtonText: "¡Seleccionar!",
                                closeOnConfirm: false
                            })

                        } else {

                            titulo_detalle = titulo_detalle + "-" + $(seleccionarDetalle[i]).val();

                            agregarGratis = true;

                        }

                    }

                }

                if (agregarGratis) {

                    window.location = rutaOculta + "index.php?ruta=finalizar-compra&gratis=true&producto=" + idProducto + "&titulo=" + titulo;

                }

            }

        }

    })
})