var emailRegex = /^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9.-]+\.[a-zA-Z0-9-]{2,24}$/;
var deviceSessionId;

$(document).ready(function() {
    OpenPay.setId($("#idOpenPay").val());
    OpenPay.setApiKey($("#llavePublicaOpenPay").val());

    if ($("#modoOpenPay").val() == 'sandbox')
        OpenPay.setSandboxMode(true);
    else {
        OpenPay.setSandboxMode(false);
    }

    if ($("#modoOpenPay").val() == 'undefined')
        OpenPay.setSandboxMode(true);

    deviceSessionId = OpenPay.deviceData.setup("payment-form", "deviceIdHiddenFieldName");
    
    var modo_invitado = localStorage.getItem("invitado");
    if (modo_invitado == "si")
        $("#mostrar_correo_electronico").show();
    else
        $("#mostrar_correo_electronico").hide();
});

$('#pay-button').on('click', function(event) {
    event.preventDefault();
    var valorItem = $(".valorItem");
    var modo_invitado = "";
    var usuario_id = "";
    var email = $(".correo_electronico").val();

    modo_invitado = localStorage.getItem("invitado");
    usuario_id = localStorage.getItem("usuario");

    if (modo_invitado == "si") {

        if (!emailRegex.test(email)) {
            swal({
                title: "Error",
                text: "para el modo invitado es necesario ingresar su correo electrónico",
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: false
            });
            return;
        } else
            $("#email").val(email);

    }

    if (!$("#PayPoliticas").prop('checked')) {
        swal({
            title: "Error",
            text: "No se an aceptado las condiciones de uso",
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false
        })
        return;
    }

    $("#pay-button").prop("disabled", true);
    OpenPay.token.extractFormAndCreate('payment-form', success_callbak, error_callbak);

});

var success_callbak = function(response) {
    var token_id = response.data.id;
    $('#token_id').val(token_id);
    EnviarServidorPago();
    //$('#payment-form').submit();
};

var error_callbak = function(response) {
    var desc = response.data.description != undefined ?
        response.data.description : response.message;
    swal({
        title: "Error",
        text: desc,
        type: "warning",
        showCancelButton: false,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Ok",
        closeOnConfirm: false
    })
    $("#pay-button").prop("disabled", false);
};


function EnviarServidorPago() {
    var ruta = rutaOculta + "ajax/enviarOpenpay.php";
    var datos = new FormData();
    var resultado;

    var titulo = $(".valorTitulo");
    var cantidad = $(".valorCantidad");
    var valorItem = $(".valorItem");
    var idProducto = $('.cuerpoCarrito button, .comprarAhora button');

    var tituloArray = [];
    var cantidadArray = [];
    var valorItemArray = [];
    var idProductoArray = [];
    var colorArray = [];
    var tallaArray = [];
    var modo_invitado = localStorage.getItem("invitado");
    var usuario_id = localStorage.getItem("usuario");
    for (var i = 0; i < titulo.length; i++) {
        tituloArray[i] = $(titulo[i]).html();
        cantidadArray[i] = $(cantidad[i]).html();
        valorItemArray[i] = $(valorItem[i]).html();
        idProductoArray[i] = $(idProducto[i]).attr("idProducto");
        colorArray[i] = $(idProducto[i]).attr("color");
        tallaArray[i] = $(idProducto[i]).attr("talla");
    }
    $pakke_adress = $("#pakke_adress").val();
    $pakke_zipCodeTo = $("#pakke_zipCodeTo").val();
    $pakke_colonia = $("#pakke_colonia").val();
    $pakke_ciudad = $("#pakke_ciudad").val();
    $pakke_estado = $("#pakke_estado").val();

    datos.append("token_id", $("#token_id").val());
    datos.append("device_session_id", deviceSessionId);
    datos.append("use_card_points", $("#use_card_points").val());
    datos.append("amount", parseFloat($("#amount").val()));
    datos.append("description", $("#description").val());
    datos.append("name", $("#name").val());
    datos.append("last_name", $("#last_name").val());
    datos.append("phone_number", $("#phone_number").val());

    datos.append("email", $("#email").val());
    if (modo_invitado == "si") {
        datos.append("modo_invitado", "si");
        datos.append("usuario_id", usuario_id);
    } else {
        datos.append("modo_invitado", "no");
    }

    datos.append("holder_name", $("#card-holder").val());
    datos.append("expiration_month", $("#expiration_month").val());
    datos.append("expiration_year", $("#expiration_year").val());
    datos.append("card_number", $("#card-number").val());
    datos.append("cvv2", $("#cvc").val());

    datos.append("tituloArray", tituloArray);
    datos.append("cantidadArray", cantidadArray);
    datos.append("valorItemArray", valorItemArray);
    datos.append("idProductoArray", idProductoArray);
    datos.append("colorArray", colorArray);
    datos.append("tallaArray", tallaArray);

    datos.append("direccionCliente", $pakke_adress);
    datos.append("coloniaCliente", $pakke_colonia);
    datos.append("cpCliente", $pakke_zipCodeTo);
    datos.append("ciudadCliente", $pakke_ciudad);
    datos.append("estadoCliente", $pakke_estado);

    datos.append("CourierCode", $("#CourierCode").val());
    datos.append("CourierName", $("#CourierName").val());
    datos.append("CourierServiceId", $("#CourierServiceId").val());
    datos.append("CourierServiceName", $("#CourierServiceName").val());
    datos.append("DeliveryDays", $("#DeliveryDays").val());
    datos.append("EstimatedDeliveryDate", $("#EstimatedDeliveryDate").val());
    datos.append("TotalPrice", $("#TotalPrice").val());
    //datos.append("ResellerServiceConfigId", $("#ResellerServiceConfigId").val());

    $.ajax({
        url: ruta,
        method: "POST",
        async: false,
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta) {
            resultado = JSON.parse(respuesta);
            localStorage.setItem("authorization_openpay", respuesta.id);
            if (resultado.error == null)
                window.location.replace(resultado.url);

            else {
                swal({
                    title: "Ocurrio un problema",
                    text: resultado.error,
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                });
            }
        },
        error: function(res) {
            swal({
                title: "Error",
                text: res.responseText,
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: false
            });
        }

    });
    return resultado;
}