<?php

require_once "conexion.php";

class ModeloPreguntas
{
    /*=============================================
	ACTUALIZAR COMENTARIO
	=============================================*/

	static public function mdlInsertarPregunta($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (usuario_id, pregunta, producto_id, fecha) VALUES (:id_usuario, :pregunta, (SELECT id FROM productos WHERE ruta = :ruta),NOW() )");

		$stmt->bindParam(":id_usuario", $datos["idUsuario"], PDO::PARAM_INT);
		$stmt->bindParam(":pregunta", $datos["pregunta"], PDO::PARAM_STR);	
		$stmt->bindParam(":ruta", $datos["ruta"], PDO::PARAM_STR);	

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;

	}

	static public function mdlMostrarPreguntas($tabla, $datos)
	{

		$stmt = Conexion::conectar()->prepare("SET GLOBAL lc_time_names = 'es_ES'");
        $stmt -> execute();
		if($datos != null)
		{
			$stmt = Conexion::conectar()->prepare("SELECT no_pregunta,producto_id,usuario_id,ifnull((select nombre from usuarios where id=usuario_id),'Invitado') as nombre_usuario,pregunta, DATE_FORMAT(fecha, '%e de %M del %Y a las %H:%i:%S') as fecha FROM $tabla WHERE producto_id = :id_producto ORDER BY fecha DESC");
			$stmt -> bindParam(":id_producto", $datos["idProducto"], PDO::PARAM_INT);
		}
		else
		{
			$stmt = Conexion::conectar()->prepare("SELECT ifnull((select portada from productos where id=$tabla.producto_id),'-') as portada, no_pregunta,producto_id,usuario_id,ifnull((select nombre from usuarios where id=$tabla.usuario_id),'Invitado') as nombre_usuario,pregunta, DATE_FORMAT(fecha, '%e de %M del %Y a las %H:%i:%S') as fecha FROM $tabla ORDER BY fecha DESC");
		}

			$stmt -> execute();

			return $stmt -> fetchAll();

		$stmt-> close();

		$stmt = null;
	}
}