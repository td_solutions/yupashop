<?php

require_once "conexion.php";
//require_once "../extensiones/PHPMailer/PHPMailerAutoload.php"; //pruebas
require_once "/home/zqbruawsva7r/public_html/extensiones/PHPMailer/PHPMailerAutoload.php";//produccion

/*=============================================
TRAER USUARIOS CON CARRITO OLVIDADO Y SIN MANDAR CORREO
=============================================*/
$tabla = "carrito_olvidado";
$pr_enviado = 'N';
$pr_horas_transcurridas = '30';
$stmt = Conexion::conectar()->prepare("SELECT DISTINCT usuario FROM $tabla WHERE usuario not LIKE 'Error%' AND correo_enviado = :enviado AND TIMESTAMPDIFF(HOUR, fecha, now()) >= :horas_transcurridas");
		$stmt->bindParam(":enviado", $pr_enviado, PDO::PARAM_STR);
		$stmt->bindParam(":horas_transcurridas", $pr_horas_transcurridas, PDO::PARAM_INT);
		$stmt -> execute();

		$respuesta = $stmt -> fetchAll();

		for($i = 0; $i < count($respuesta); $i++){
			envio_correo($respuesta[$i]["usuario"]);
		}

function envio_correo($username){

/*=============================================
	ENVIO CORREO RECORDATORIO CARRITO
	=============================================*/
	$consulta_nombre_usuario = "";
	$respuesta_productos_correo;
	$pr_estatus = "0";

	$tabla = "usuarios";
	$stmt = Conexion::conectar()->prepare("SELECT nombre FROM $tabla WHERE email = :email AND verificacion = :estatus");
	$stmt->bindParam(":email", $username, PDO::PARAM_STR);
	$stmt->bindParam(":estatus", $pr_estatus, PDO::PARAM_INT);
	$stmt -> execute();

	$consulta_nombre_usuario = $stmt -> fetch();

	$tabla = "productos";
	$stmt = Conexion::conectar()->prepare("SELECT titulo,portada,precio FROM $tabla WHERE id in ( SELECT producto_id FROM `carrito_olvidado` WHERE usuario = :username )");
	$stmt->bindParam(":username", $username, PDO::PARAM_STR);
	$stmt -> execute();

	$respuesta_productos_correo = $stmt -> fetchAll();
	$htmlforprod = '';
	for($i = 0; $i < count($respuesta_productos_correo); $i++)
	{
		$htmlforprod .= '<table><tr><td align="center" valign="top" style="width:150px;height:115px">
		<img border="0" src=https://www.yupashop.com/backend/'. $respuesta_productos_correo[$i]["portada"] .' style="width:120px;height:80px">
		</td><td valign="top" style="color:#666;padding:10px 10px 0 10px;width:330px;height:115px">
		<span style="font:14px Arial,sans-serif;text-decoration:none;color:#006699">'.$respuesta_productos_correo[$i]["titulo"].'</span>
		</td><td align="left" valign="top" style="font:14px Arial,san-serif;padding:10px 0 0 0;width:80px;height:115px"><strong>$'.$respuesta_productos_correo[$i]["precio"].'</strong></td>
		</tr></table>';
	}
	date_default_timezone_set("America/Mexico_City");

	//$url = Ruta::ctrRuta();	

	$mail = new PHPMailer;

	$mail->CharSet = 'UTF-8';

	$mail->isMail();

	$mail->setFrom('noreply@yupashop.com', 'YUPASHOP');					


	$mail->Subject = "Tus compras te estan esperando en tu carrito ==YUPASHOP=";

	$mail->addAddress($username);

	$mail->msgHTML('
		<div style="width:100%; background:#eee; position:relative; font-family:sans-serif; padding-bottom:40px">
		<center><img style="padding:20px; width:10%" src="https://www.yupashop.com/tienda/logo.png"></center>
		<div style="position:relative; margin:auto; width:600px; background:white; padding-bottom:20px">
			<center>
			<img style="padding-top:20px; width:15%" src="https://www.yupashop.com/tienda/icon-email.png">
			<h3 style="font-weight:100; color:#999;">Hola '.$consulta_nombre_usuario[0].': 
			Notamos que agregaste uno o varios productos a tu Carrito de Compra pero no la finalizaste. Cuando estés listo visita tu Carrito de Compra para completar tu orden..</h3>
			<hr style="width:80%; border:1px solid #ccc">
			'.$htmlforprod.'
			</center>
		</div>
	</div>');
	
	try {
	$envio = $mail->Send();
	if($envio){
		actualizar_envio_correo($username);
	}
	} catch (\Throwable $th) {
		error_envio_correo($th);
	}
}

	function actualizar_envio_correo($pr_usuario){
		$tabla = "carrito_olvidado";
		$pr_enviado = 'S';

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla set correo_enviado = :enviado WHERE usuario = :usuario");
	
		$stmt->bindParam(":usuario", $pr_usuario, PDO::PARAM_STR);
		$stmt->bindParam(":enviado", $pr_enviado, PDO::PARAM_STR);
	
		$stmt->execute();
	}


	function error_envio_correo($error){
		$tabla = "carrito_olvidado";
		$pr_producto_id = 123;
		$pr_cantidad = 1;
		$pr_enviado = 'N';

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(usuario, producto_id, cantidad, fecha, correo_enviado) VALUES (:usuario, :producto_id, :cantidad, NOW(), :enviado)");
	
		$stmt->bindParam(":usuario", $error, PDO::PARAM_STR);
		$stmt->bindParam(":producto_id", $pr_producto_id, PDO::PARAM_INT);
		$stmt->bindParam(":cantidad", $pr_cantidad, PDO::PARAM_INT);
		$stmt->bindParam(":enviado", $pr_enviado, PDO::PARAM_STR);
		$stmt->execute();
	}
