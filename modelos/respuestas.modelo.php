<?php

require_once "conexion.php";

class ModeloRespuestas
{
    /*=============================================
	ACTUALIZAR COMENTARIO
	=============================================*/

	static public function mdlInsertarRespuesta($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (usuario_id, pregunta, producto_id, fecha) VALUES (:id_usuario, :pregunta, (SELECT id FROM productos WHERE ruta = :ruta),NOW() )");

		$stmt->bindParam(":id_usuario", $datos["idUsuario"], PDO::PARAM_INT);
		$stmt->bindParam(":pregunta", $datos["pregunta"], PDO::PARAM_STR);	
		$stmt->bindParam(":ruta", $datos["ruta"], PDO::PARAM_STR);	

		if($stmt -> execute()){

			return "ok";

		}else{

			return "error";

		}

		$stmt-> close();

		$stmt = null;

	}

	static public function mdlMostrarRespuestas($tabla, $datos)
	{
        $stmt = Conexion::conectar()->prepare("SET GLOBAL lc_time_names = 'es_ES'");
        $stmt -> execute();
		$stmt = Conexion::conectar()->prepare("SELECT no_respuesta,no_pregunta,respuesta,DATE_FORMAT(fecha, '%e de %M del %Y a las %H:%i:%S') as fecha FROM $tabla WHERE no_pregunta = :id_pregunta ORDER BY fecha");

			$stmt -> bindParam(":id_pregunta", $datos["no_pregunta"], PDO::PARAM_INT);

			$stmt -> execute();

			return $stmt -> fetchAll();

		$stmt-> close();

		$stmt = null;
	}
}