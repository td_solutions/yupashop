<?php

require_once "conexion.php";

class ModeloCarrito{

	/*=============================================
	MOSTRAR TARIFAS
	=============================================*/

	static public function mdlMostrarTarifas($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla");

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$tmt =null;

	}

	/*=============================================
	NUEVAS COMPRAS
	=============================================*/

	static public function mdlNuevasCompras($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla (id_usuario,
		 id_producto,
		 metodo,
		 email,
		 direccion,
		 pais,
		 pago,
		 No_Pedido,
		 talla,
		 color) VALUES (
			 :id_usuario, 
			 :id_producto, 
			 :metodo, 
			 :email, 
			 :direccion, 
			 :pais, 
			 :pago, 
			 :pedido,
			 :talla,
			 :color
			 )");

		$stmt->bindParam(":id_usuario", $datos["idUsuario"], PDO::PARAM_INT);
		$stmt->bindParam(":id_producto", $datos["idProducto"], PDO::PARAM_INT);
		$stmt->bindParam(":metodo", $datos["metodo"], PDO::PARAM_STR);
		$stmt->bindParam(":email", $datos["email"], PDO::PARAM_STR);
		$stmt->bindParam(":direccion", $datos["direccion"], PDO::PARAM_STR);
		$stmt->bindParam(":pais", $datos["pais"], PDO::PARAM_STR);
		$stmt->bindParam(":pago", $datos["pago"], PDO::PARAM_STR);
		$stmt->bindParam(":pedido", $datos["No_Pedido"], PDO::PARAM_STR);		
		$stmt->bindParam(":talla", $datos["talla"], PDO::PARAM_STR);
		$stmt->bindParam(":color", $datos["color"], PDO::PARAM_STR);

		if($stmt->execute()){ 

			return "ok"; 

		}else{ 

			return "error"; 

		}

		$stmt->close();

		$tmt =null;
	}

	/*=============================================
	NUEVOS PEDIDOS
	=============================================*/

	static public function mdlNuevasPedidos($tabla, $datos){
		$db = Conexion::conectar();
		$stmt = $db->prepare("INSERT INTO $tabla (Pakke_CourierCode, 
		Pakke_CourierName, 
		Pakke_CourierServiceId, 
		Pakke_CourierServiceName, 
		Pakke_DeliveryDays, 
		Pakke_EstimatedDeliveryDate, 
		Pakke_TotalPrice, 
		Pakke_ResellerReference,
		No_Guia,
		Fecha,
		Estatus,
		ShipmentId,
		calle_numero,
		colonia,
		cp,
		ciudad,
		estado ) 
		VALUES (:Pakke_CourierCode, 
		:Pakke_CourierName, 
		:Pakke_CourierServiceId, 
		:Pakke_CourierServiceName, 
		:Pakke_DeliveryDays, 
		:EstimatedDeliveryDate, 
		:Pakke_TotalPrice, 
		:Pakke_ResellerReference, 
		:No_Guia, 
		:Fecha, 
		:Estatus, 
		:ShipmentId,
		:calle_numero,
		:colonia,
		:cp,
		:ciudad,
		:estado)");

		$stmt->bindParam(":Pakke_CourierCode", $datos["CourierCode"], PDO::PARAM_STR);
		$stmt->bindParam(":Pakke_CourierName", $datos["CourierName"], PDO::PARAM_STR);
		$stmt->bindParam(":Pakke_CourierServiceId", $datos["CourierServiceId"], PDO::PARAM_STR);
		$stmt->bindParam(":Pakke_CourierServiceName", $datos["CourierServiceName"], PDO::PARAM_STR);
		$stmt->bindParam(":Pakke_DeliveryDays", $datos["DeliveryDays"], PDO::PARAM_STR);
		$stmt->bindParam(":EstimatedDeliveryDate", $datos["EstimatedDeliveryDate"], PDO::PARAM_STR);
		$stmt->bindParam(":Pakke_TotalPrice", $datos["TotalPrice"], PDO::PARAM_STR);
		$stmt->bindParam(":Pakke_ResellerReference", $datos["ResellerServiceConfigId"], PDO::PARAM_STR);

		$stmt->bindParam(":No_Guia", $datos["No_Guia"], PDO::PARAM_STR);
		$stmt->bindParam(":Fecha", $datos["fecha"], PDO::PARAM_STR);
		$stmt->bindParam(":Estatus", $datos["estatus"], PDO::PARAM_STR);
		$stmt->bindParam(":ShipmentId", $datos["ShipmentId"], PDO::PARAM_STR);

		$stmt->bindParam(":calle_numero", $datos["calle_numero"], PDO::PARAM_STR);
		$stmt->bindParam(":colonia", $datos["colonia"], PDO::PARAM_STR);
		$stmt->bindParam(":cp", $datos["cp"], PDO::PARAM_STR);
		$stmt->bindParam(":ciudad", $datos["ciudad"], PDO::PARAM_STR);
		$stmt->bindParam(":estado", $datos["estado"], PDO::PARAM_STR);

		if($stmt->execute()){

			return $db->lastInsertId();

		}else{ 

			return 0; 

		}

		$stmt->close();

		$tmt =null;
	}
	/*=============================================
	VERIFICAR PRODUCTO COMPRADO
	=============================================*/

	static public function mdlVerificarProducto($tabla, $datos){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE id_usuario = :id_usuario AND id_producto = :id_producto");

		$stmt->bindParam(":id_usuario", $datos["idUsuario"], PDO::PARAM_INT);
		$stmt->bindParam(":id_producto", $datos["idProducto"], PDO::PARAM_INT);

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$tmt =null;

	}

}