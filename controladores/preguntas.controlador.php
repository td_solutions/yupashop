<?php

class ControladorPreguntas
{

    /*=============================================
	GUARDAR PREGUNTA
	=============================================*/

	static public function ctrRegistroPreguntas($datos){

		$tabla = "preguntas";
		$pregunta = $datos["pregunta"];
		$usuario = $datos["idUsuario"];
		$ruta = $datos["ruta"];

		$respuesta = ModeloPreguntas::mdlInsertarPregunta($tabla, $datos);

		return $respuesta;

    }
    /*=============================================
	MOSTRAR PREGUNTAS
	=============================================*/

	static public function ctrMostrarPreguntas($datos){

		$tabla = "preguntas";

		$respuesta = ModeloPreguntas::mdlMostrarPreguntas($tabla, $datos);

		return $respuesta;

	}


}