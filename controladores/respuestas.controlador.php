<?php

class ControladorRespuestas
{

    /*=============================================
	GUARDAR RESPUESTA
	=============================================*/

	static public function ctrRegistroRespuestas($datos){

		$tabla = "respuestas";
		$pregunta = $datos["respuesta"];
		$usuario = $datos["no_pregunta"];

		$respuesta = ModeloRespuestas::mdlInsertarRespuestas($tabla, $datos);

		return $respuesta;

    }
    /*=============================================
	MOSTRAR RESPUESTAS
	=============================================*/

	static public function ctrMostrarRespuestas($datos){

		$tabla = "respuestas";

		$respuesta = ModeloRespuestas::mdlMostrarRespuestas($tabla, $datos);

		return $respuesta;

	}


}