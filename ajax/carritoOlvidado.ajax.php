<?php
//require_once "../modelos/conexion.php";
require_once "/home/zqbruawsva7r/public_html/modelos/conexion.php";//produccion

class AjaxCarritoOlvidado{
    public $idUsuario;
	public $idProducto;
	public $cantidad;
    
    function insertar_producto(){

		if($this->idUsuario != ""){
			$tabla = "usuarios";
			$stmt = Conexion::conectar()->prepare("SELECT email FROM $tabla WHERE id = :id_usuario ");

			$stmt -> bindParam(":id_usuario", $this->idUsuario, PDO::PARAM_INT);

			$stmt -> execute();

			$resultado_consulta = $stmt -> fetch();

			$email = $resultado_consulta[0];

			$tabla = "carrito_olvidado";
			$pr_enviado = 'N';

			$stmt = Conexion::conectar()->prepare("INSERT INTO $tabla(usuario, producto_id, cantidad, fecha, correo_enviado) VALUES (:usuario, :producto_id, :cantidad, NOW(), :enviado)");

			$stmt->bindParam(":usuario", $email, PDO::PARAM_STR);
			$stmt->bindParam(":producto_id", $this->idProducto, PDO::PARAM_INT);
			$stmt->bindParam(":cantidad", $this->cantidad, PDO::PARAM_INT);
			$stmt->bindParam(":enviado", $pr_enviado, PDO::PARAM_STR);
			$stmt->execute();
		}

        
	}
	function borrar_producto(){

		if($this->idUsuario != ""){
			$tabla = "usuarios";
			$stmt = Conexion::conectar()->prepare("SELECT email FROM $tabla WHERE id = :id_usuario ");

			$stmt -> bindParam(":id_usuario", $this->idUsuario, PDO::PARAM_INT);

			$stmt -> execute();

			$resultado_consulta = $stmt -> fetch();

			$email = $resultado_consulta[0];

			$tabla = "carrito_olvidado";
			$pr_enviado = 'N';

			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla where usuario = :usuario AND producto_id = :producto_id");

			$stmt->bindParam(":usuario", $email, PDO::PARAM_STR);
			$stmt->bindParam(":producto_id", $this->idProducto, PDO::PARAM_INT);
			$stmt->execute();
		}
	}
	function borrar_carrito()
	{
		if($this->idUsuario != ""){
			$tabla = "usuarios";
			$stmt = Conexion::conectar()->prepare("SELECT email FROM $tabla WHERE id = :id_usuario ");
			$stmt -> bindParam(":id_usuario", $this->idUsuario, PDO::PARAM_INT);
			$stmt -> execute();

			$resultado_consulta = $stmt -> fetch();
			$email = $resultado_consulta[0];
			$tabla = "carrito_olvidado";

			$stmt = Conexion::conectar()->prepare("DELETE FROM $tabla where usuario = :usuario");

			$stmt->bindParam(":usuario", $email, PDO::PARAM_STR);
			$stmt->execute();
		}
    }
}

if(isset($_POST["agregarCarritoOlvidado"])){
	$deseo = new AjaxCarritoOlvidado();
	$deseo -> idUsuario = $_POST["idUsuario"];
    $deseo -> idProducto = $_POST["idProducto"];
    $deseo -> cantidad = $_POST["cantidad"];
	$deseo ->insertar_producto();
}

if(isset($_POST["quitarCarritoOlvidado"])){
	$deseo = new AjaxCarritoOlvidado();
	$deseo -> idUsuario = $_POST["idUsuario"];
    $deseo -> idProducto = $_POST["idProducto"];
	$deseo ->borrar_producto();
}
if(isset($_POST["borrar_carrito_usuario"])){
	$deseo = new AjaxCarritoOlvidado();
	$deseo -> idUsuario = $_POST["idUsuario"];
	$deseo ->borrar_carrito();
}