<?php
require dirname(__DIR__) .'/extensiones/vendor/openpay/sdk/Openpay.php';
require dirname(__DIR__) .'/modelos/carrito.modelo.php';
require dirname(__DIR__) .'/controladores/usuarios.controlador.php';
require dirname(__DIR__) .'/modelos/usuarios.modelo.php';


class AjaxOpenpay
{
    public $idProductos;
    public $cantidadProductos;
    public $tallasProductos;
    public $coloresProductos;
    public $pagoProductos;
    
    public $direccionCliente;
    public $coloniaCliente;
    public $cpCliente;
    public $ciudadCliente;
    public $estadoCliente;
    public $CourierCode;
    public $CourierName;
    public $CourierServiceId;
    public $CourierServiceName;
    public $DeliveryDays;
    public $EstimatedDeliveryDate;
    public $TotalPrice;
    
    public $name;
    public $last_name;
    public $phone_number;
    public $email;
    
    public $source_id;
    public $amount;
    public $description;
    public $device_session_id;
    
    public $modo_invitado;
    public $usuario_id;
    
    public function ajaxEnviarOpenPay(){
      session_start();    
      $tabla = "comercio";
      $respuesta = ModeloCarrito::mdlMostrarTarifas($tabla);
      $idOpenPay = $respuesta["accountIdPayu"];

      $llavePrivadaOpenPay = $respuesta["apiKeyPayu"];
      $llavePublicaOpenPay = $respuesta["merchantIdPayu"];
        
      $_SESSION["direccionCliente"] =  $this->direccionCliente;
      $_SESSION["coloniaCliente"] =  $this->coloniaCliente;
      $_SESSION["cpCliente"] =  $this->cpCliente;
      $_SESSION["ciudadCliente"] =  $this->ciudadCliente;
      $_SESSION["estadoCliente"] =  $this->estadoCliente;
    
      $_SESSION["CourierCode"] =  $this->CourierCode;
      $_SESSION["CourierName"] =  $this->CourierName;
      $_SESSION["CourierServiceId"] =  $this->CourierServiceId;
      $_SESSION["CourierServiceName"] =  $this->CourierServiceName;
      $_SESSION["DeliveryDays"] =  $this->DeliveryDays;
      $_SESSION["EstimatedDeliveryDate"] =  $this->EstimatedDeliveryDate;
      $_SESSION["TotalPrice"] =  $this->TotalPrice;
      
      $this->name = $_SESSION["nombre"];
      if($this->modo_invitado == "si"){
        $_SESSION["direccionCliente"] = $this->direccionCliente;
        $_SESSION["email"] = $this->email;
        $_SESSION["id"] = $this->usuario_id;

        ControladorUsuarios::ctrActualizarUsuario($this->usuario_id,"email",$this->email);
      }
      else{
        $this->email = $_SESSION["email"];
      }
        
        
      $modoOpenPay = $respuesta["modoPayu"];
      
      if($modoOpenPay === 'sandbox'){
        Openpay::setProductionMode(false); 
        Openpay::setId($idOpenPay);
        Openpay::setApiKey($llavePrivadaOpenPay);
        }
        else
        {
            Openpay::setProductionMode(true); 
        Openpay::setId($idOpenPay);
        Openpay::setApiKey($llavePrivadaOpenPay);
            
        }
      $openpay = Openpay::getInstance($idOpenPay,$llavePrivadaOpenPay);
        
    
    
    $customer = array(
     'name' => $this->name,
     'last_name' => $this->last_name,
     'phone_number' => $this->phone_number,
     'email' => $this->email);

    $chargeData = array(
    'method' => 'card',
    'source_id' => $this->token_id,
    'amount' => $this->amount,
    'description' => $this->description,
    'device_session_id' => $this->device_session_id,
    'redirect_url' => 'https://www.yupashop.com/index.php?ruta=finalizar-compra-payu&openpay=true&productos='.$this->idProductos.'&cantidad='.$this->cantidadProductos.'&pago='.$this->pagoProductos.'&tallas='.$this->tallasProductos.'&colores='.$this->coloresProductos,
    'use_3d_secure'=> true,
    'customer' => $customer
    );
    try {
    $charge = $openpay->charges->create($chargeData);
    $datos_respuesta = array("id" => $charge->id, "estatus" => $charge->status, "error" => $charge->error_message, "url" => $charge->payment_method->url);
    } catch (Exception $e) {
     $datos_respuesta = array("id" => 0, "estatus" => 'error', "error" => $e->getMessage(), "url" => '');    
    }
    
    
echo json_encode($datos_respuesta);        
    }

}

/*=============================================
MÉTODO OPENPAY
=============================================*/
if(isset($_POST["device_session_id"])){
	$payu = new AjaxOpenpay();
	$payu -> idProductos = str_replace(",","-", $_POST["idProductoArray"]);
  $payu -> cantidadProductos = str_replace(",","-", $_POST["cantidadArray"]);
  $payu -> pagoProductos = str_replace(",","-", $_POST["valorItemArray"]);

  $payu -> tallasProductos = str_replace(",","-", $_POST["tallaArray"]);
  $payu -> coloresProductos = str_replace(",","-", $_POST["colorArray"]);

  $payu -> direccionCliente =  $_POST["direccionCliente"];
  $payu -> coloniaCliente =  $_POST["coloniaCliente"];
  $payu -> cpCliente =  $_POST["cpCliente"];
  $payu -> ciudadCliente =  $_POST["ciudadCliente"];
  $payu -> estadoCliente =  $_POST["estadoCliente"];

  $payu -> CourierCode =  $_POST["CourierCode"];
  $payu -> CourierName =  $_POST["CourierName"];
  $payu -> CourierServiceId =  $_POST["CourierServiceId"];
  $payu -> CourierServiceName =  $_POST["CourierServiceName"];
  $payu -> DeliveryDays =  $_POST["DeliveryDays"];
  $payu -> EstimatedDeliveryDate =  $_POST["EstimatedDeliveryDate"];
  $payu -> TotalPrice =  $_POST["TotalPrice"];
  if(isset($_POST["email"]))
  $payu -> email = $_POST["email"];
  if(isset($_POST["modo_invitado"]))
  $payu -> modo_invitado = $_POST["modo_invitado"];
  if(isset($_POST["usuario_id"]))
  $payu -> usuario_id = $_POST["usuario_id"];
  
  $payu -> token_id = $_POST["token_id"];
  $payu -> amount = $_POST["amount"];
  $payu -> description = $_POST["description"];
  $payu -> device_session_id = $_POST["device_session_id"];    
	$payu -> ajaxEnviarOpenPay();
}

?>