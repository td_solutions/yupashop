<?php

require_once "../controladores/preguntas.controlador.php";
require_once "../modelos/preguntas.modelo.php";

class PreguntasRespuestas{
    public $preguntaUsuario;
    public $idProducto;
    public $idUsuario;
    public $ruta;

    public function ajaxRegistrarPregunta()
    {
        $datos = array("idUsuario"=>$this->idUsuario,
                       "ruta"=>$this->ruta,
                       "pregunta"=>$this->preguntaUsuario);
        $respuesta = ControladorPreguntas::ctrRegistroPreguntas($datos);
        echo json_encode($respuesta);
    }
    
}

/*=============================================
VALIDAR EMAIL EXISTENTE
=============================================*/	

if(isset($_POST["preguntaUsuario"])){

	$peticionPreguntas = new PreguntasRespuestas();
    $peticionPreguntas -> preguntaUsuario = $_POST["preguntaUsuario"];
    $peticionPreguntas -> idUsuario = $_POST["idUsuario"];
    $peticionPreguntas -> ruta = $_POST["ruta"];
	$peticionPreguntas -> ajaxRegistrarPregunta();

}