<?php

require_once "../controladores/pakke.controlador.php";
require_once "../modelos/pakke.modelo.php";

class AjaxPakke{
/*=============================================
	OBTENER PAKKE API KEY
	=============================================*/	

	public $validarEmail;

	public function ajaxObtenerKey(){

		$datos = $this->validarEmail;

		$respuesta = ControladorPakke::ctrObtenerApiKey();

		echo json_encode($respuesta);

	}
	/*=============================================
OBTENER TASA MINIMA PARA ENVIO GRATIS
=============================================*/	
	public function ajaxObtenerTasaMinimaEnvioGratis(){

		$datos = $this->validarEmail;

		$respuesta = ControladorPakke::ctrObtenerTasaMinimaEnvioGratis();

		echo json_encode($respuesta);

	}
}
/*=============================================
OBTENER PAKKE API KEY
=============================================*/	

if(isset($_POST["accion"])){
    $accion = $_POST["accion"];
    if($accion === "obtenerPakkeApiKey")
    {
	    $pakkeKey = new AjaxPakke();
        $pakkeKey -> ajaxObtenerKey();
    }
}
/*=============================================
OBTENER TASA MINIMA PARA ENVIO GRATIS
=============================================*/	
if(isset($_POST["accion"])){
    $accion = $_POST["accion"];
    if($accion === "obtenerTasaMinimaEnvioGratis")
    {
	    $pakkeKey = new AjaxPakke();
        $pakkeKey -> ajaxObtenerTasaMinimaEnvioGratis();
    }
}