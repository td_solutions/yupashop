<?php
require_once "../modelos/conexion.php";
require_once "../modelos/usuarios.modelo.php";
class Descuentos{
    public $nombre = "Invitado";
	public $idProducto;
	public $cantidad;

	public function traer_descuento(){
        session_start();  
        if(!isset($_SESSION["id"])){
            echo json_encode("N");
            return;
		}
		if(isset($_SESSION["nombre"]))
		{
			if($_SESSION["nombre"] === "Invitado")
			{
            	echo json_encode("N");
				return;
			}
        }
        $id = $_SESSION["id"];
		$tabla = "usuarios";
		$respuesta = ModeloUsuarios::mdlConsultarDescuentoPrimeraCompra($id);
		echo json_encode($respuesta);
	}
}
if(isset($_POST["descuento"])){
	$deseo = new Descuentos();
	$deseo ->traer_descuento();
}