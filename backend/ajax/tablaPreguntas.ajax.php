<?php

require_once "../../controladores/preguntas.controlador.php";
require_once "../../modelos/preguntas.modelo.php";

class TablaPreguntas{

  /*=============================================
  MOSTRAR LA TABLA DE CATEGORÍAS
  =============================================*/ 

 	public function mostrarTabla(){	

 	$item = null;
 	$valor = null;

 	$preguntas = ControladorPreguntas::ctrMostrarPreguntas($item, $valor);	

 	if(count($preguntas) == 0){

      $datosJson = '{ "data":[]}';

      echo $datosJson;

      return;

    }

 	$datosJson = '{
		 
		  "data": [ ';

	for($i = 0; $i < count($preguntas); $i++){
	
			/*=============================================
			REVISAR ESTADO
			=============================================*/ 

			// if( $categorias[$i]["estado"] == 0){
				
			// 	$colorEstado = "btn-danger";
			// 	$textoEstado = "Desactivado";
			// 	$estadoCategoria = 1;

			// }else{

				$colorEstado = "btn-success";
				$textoEstado = "Activado";
				$estadoCategoria = 0;

			//}

		 	$estado = "<button class='btn ".$colorEstado." btn-xs btnActivar' estadoCategoria='".$estadoCategoria."' idCategoria='".$preguntas[$i]["no_preguta"]."'>".$textoEstado."</button>";

		 	/*=============================================
			REVISAR IMAGEN PORTADA
			=============================================*/ 
    		 $imgPortada = "<img class='img-thumbnail imgPortadaCategorias' src='".$preguntas[$i]["portada"]."' width='100px'>";


			/*=============================================
  			CREAR LAS ACCIONES
  			=============================================*/
	    
		    $acciones = "<div class='btn-group'><button class='btn btn-warning btnEditarCategoria' idCategoria='".$preguntas[$i]["id"]."' data-toggle='modal' data-target='#modalEditarCategoria'><i class='fa fa-pencil'></i></button><button class='btn btn-danger btnEliminarCategoria' idCategoria='".$categorias[$i]["id"]."' imgPortada='".$cabeceras["portada"]."'  rutaCabecera='".$categorias[$i]["ruta"]."' imgOferta='".$categorias[$i]["imgOferta"]."'><i class='fa fa-times'></i></button></div>";
				    
			$datosJson	 .= '[
					"'.$imgPortada.'",
				      "'.$preguntas[$i]["no_pregunta"].'",
					  "'.$preguntas[$i]["producto_id"].'",
					  "'.$preguntas[$i]["usuario_id"].'",
				      "'.$preguntas[$i]["nombre_usuario"].'",
					  "'.$preguntas[$i]["pregunta"].'",
					  "'.$preguntas[$i]["fecha"].'",
					  "'.($i+1).'",
				      "'.$acciones.'"		    
				    ],';

	}

	$datosJson = substr($datosJson, 0, -1);

	$datosJson.=  ']
		  
	}'; 

	echo $datosJson;


 	}


}

/*=============================================
ACTIVAR TABLA DE CATEGORÍAS
=============================================*/ 
$activar = new TablaPreguntas();
$activar -> mostrarTabla();