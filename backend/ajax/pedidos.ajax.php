<?php
error_reporting(0);

require_once "../controladores/ventas.controlador.php";
require_once "../modelos/ventas.modelo.php";
require_once "../../extensiones/PHPMailer/PHPMailerAutoload.php";
require_once "../../extensiones/vendor/autoload.php";


class AjaxPedidos{
/*=============================================
	TRAER PEDIDO
	=============================================*/	
	public $guia;
	public $no_envio;
	public $emailUsuario;
	public $paqueteria;
	public $productosImagenes;

	public function ajaxTraerPedido(){

		$item = "No_Pedido";
		$valor = $this->no_envio;

		$respuesta = ControladorVentas::ctrMostrarPedido($item, $valor);

		echo json_encode($respuesta);

    }
    
    public function ajaxTraerListaProd(){

		$item = "No_Pedido";
		$valor = $this->no_envio;

		$respuesta = ControladorVentas::ctrMostrarProductos($item, $valor);

		echo json_encode($respuesta);

	}
	public function ajaxSetGuia(){

		$respuesta = ModeloVentas::mdlActualizarVenta("pedidos", "No_Guia", $this->guia, "No_Pedido", $this->no_envio);
		$respuesta = ModeloVentas::mdlActualizarVenta("compras", "envio", "1", "No_Pedido", $this->no_envio);
		$this-> enviarCorreo();
		//echo $respuesta;
}

public function ajaxFinalizarGuia(){

	//$respuesta = ModeloVentas::mdlActualizarVenta("pedidos", "No_Guia", $this->guia, "No_Pedido", $this->no_envio);
	$respuesta = ModeloVentas::mdlActualizarVenta("compras", "envio", "2", "No_Pedido", $this->no_envio);
	$this-> enviarCorreo();
	//echo $respuesta;
}

public function enviarCorreo(){
	//enviar

				/*=============================================
				VERIFICACIÓN CORREO ELECTRÓNICO
				=============================================*/

				date_default_timezone_set("America/Mexico_City");


				$mail = new PHPMailer;

				$mail->CharSet = 'UTF-8';

				$mail->isMail();

				$mail->setFrom('tienda@yupashop.com', 'Yupashop');

				$mail->addReplyTo('tienda@yupashop.com', 'Yupashop');

				$mail->Subject = "Producto en camino!";

				$mail->addAddress($this->emailUsuario);



				$mail->msgHTML('<div style="width:100%; background:#eee; position:relative; font-family:sans-serif; padding-bottom:40px">
					
					<center>
						
						<img style="padding:20px; width:10%" src="https://www.yupashop.com/tienda/logo.png">

					</center>

					<div style="position:relative; margin:auto; width:600px; background:white; padding:20px">
					
						<center>
						
						<img style="padding:20px; width:15%" src="https://www.yupashop.com/tienda/icon-email.png">

						<h3 style="font-weight:100; color:#999">LA GUIA DE ENVIO SE GENERO EXITOSAMENTE</h3>

						<hr style="border:1px solid #ccc; width:80%">

						<h4 style="font-weight:100; color:#999; padding:0 20px">No. Guia '. $this->guia . '</h4>
						<h4 style="font-weight:100; color:#999; padding:0 20px">Paquetería '. $this->paqueteria . '</h4>
						<a href="https://www.yupashop.com/perfil">
						<div style="line-height:60px; background:#0aa; width:60%; color:white">Con este número podrá dar seguimiento a su envío</div>
						</a>]
						</br>
						<table width="300" cellspacing="0" style="font:12px/16px Arial,sans-serif;color:rgb(51,51,51);background-color:rgb(255,255,255);margin:-20" cellpadding="0"> 
							<tr>
							<td colspan="2" style="width:600px">
							<p style="font:18px Arial,sans-serif;color:#cc6600;margin:10px 20px 3px 20px;border-bottom:1px solid #ccc;padding:0 0 3px 0"> Detalles del envío </p>							
							</tr>
							<tr> 
     								<td colspan="2" style="padding:0px 40px;width:600px"> 
      									<table width="560" cellspacing="0" cellpadding="0"> 
       										<tbody> 


							'.$this->productosImagenes.'



							<table>
							</td> 
						</tr> 
							 <table>
						</center>

					</div>

				</div>');

				$envio = $mail->Send();
				// dormir durante 10 segundos
sleep(3);
//echo $envio;
				//if(!$envio)
	//correo
	}
}

/*=============================================
CONSULTAR PEDIDO
=============================================*/
if(isset($_POST["datos_pedido"])){
    $activarPedido = new AjaxPedidos();
	$activarPedido -> no_envio = $_POST["no_pedido"];
	$activarPedido -> ajaxTraerPedido();
}
if(isset($_POST["lista_productos"])){
    $activarPedido = new AjaxPedidos();
	$activarPedido -> no_envio = $_POST["no_pedido"];
	$activarPedido -> ajaxTraerListaProd();

}

if(isset($_POST["TrackingNumber"])){
	$activarPedido = new AjaxPedidos();
	$activarPedido -> no_envio = $_POST["no_pedido"];
	$activarPedido -> paqueteria = $_POST["CourierName"];
	$activarPedido -> guia = $_POST["TrackingNumber"];
	$activarPedido -> emailUsuario = $_POST["emailUsuario"];
	$activarPedido -> productosImagenes = $_POST["contenidoImagenes"];
	$activarPedido -> ajaxSetGuia();	
}

if(isset($_POST["terminado"])){
	$activarPedido = new AjaxPedidos();
	$activarPedido -> no_envio = $_POST["no_pedido"];
	$activarPedido -> ajaxFinalizarGuia();	

}