<?php

class ControladorVentas{

	/*=============================================
	MOSTRAR TOTAL VENTAS
	=============================================*/

	static public function ctrMostrarTotalVentas(){

		$tabla = "compras";

		$respuesta = ModeloVentas::mdlMostrarTotalVentas($tabla);

		return $respuesta;

	}

	/*=============================================
	MOSTRAR VENTAS
	=============================================*/

	static public function ctrMostrarVentas(){

		$tabla = "compras";

		$respuesta = ModeloVentas::mdlMostrarVentas($tabla);

		return $respuesta;

	}

	/*=============================================
	MOSTRAR PEDIDO
	=============================================*/

	static public function ctrMostrarPedido($item,$valor1){

		$tabla = "pedidos";		
		$respuesta = ModeloVentas::mdlMostrarPedidos($tabla,$item,$valor1);
		return $respuesta;

	}
	/*=============================================
	MOSTRAR PRODUCTOS
	=============================================*/

	static public function ctrMostrarProductos($item,$valor1){

		$respuesta = ModeloVentas::mdlMostrarProductos($item,$valor1);
		return $respuesta;

	}

	/*=============================================
	MOSTRAR PRODUCTOS
	=============================================*/

	static public function ctrActualizarGuia($item,$valor1){

		$respuesta = ModeloVentas::mdlActualizarVenta($item,$valor1);
		return $respuesta;

	}
	
	

}