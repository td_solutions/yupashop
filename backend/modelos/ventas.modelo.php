<?php

require_once "conexion.php";

class ModeloVentas{

	/*=============================================
	MOSTRAR EL TOTAL DE VENTAS
	=============================================*/	

	static public function mdlMostrarTotalVentas($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT SUM(pago) as total FROM $tabla");

		$stmt -> execute();

		return $stmt -> fetch();

		$stmt -> close();

		$stmt = null;

	}

	/*=============================================
	MOSTRAR VENTAS
	=============================================*/	

	static public function mdlMostrarVentas($tabla){

		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla ORDER BY id DESC");

		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}
	/*=============================================
	MOSTRAR PEDIDOS
	=============================================*/	

	static public function mdlMostrarPedidos($tabla, $item1, $valor1){
		$stmt = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE $item1 = :$item1");
		$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}
	/*=============================================
	MOSTRAR PRODUCTOS
	=============================================*/	

	static public function mdlMostrarProductos($item1, $valor1){ 	
		//$stmt = Conexion::conectar()->prepare("SELECT titulo,peso,longitud,ancho,alto,portada,precio,cantidad,IFNULL((select talla from compras WHERE No_Pedido = :pedido AND id_producto=productos.id  LIMIT 1),'0') AS talla,IFNULL((select color from compras WHERE No_Pedido = :pedido and id_producto=productos.id  LIMIT 1),'0') AS color FROM productos WHERE id in (select id_producto from compras WHERE No_Pedido = :pedido)");
		$stmt = Conexion::conectar()->prepare("SELECT * FROM compras c,productos p WHERE p.id= c.id_producto AND c.No_Pedido = :pedido");
		$stmt -> bindParam(":pedido", $valor1, PDO::PARAM_STR);
		$stmt -> execute();

		return $stmt -> fetchAll();

		$stmt -> close();

		$stmt = null;

	}
	/*=============================================
	ACTUALIZAR ENVIO VENTA
	=============================================*/

	static public function mdlActualizarVenta($tabla, $item1, $valor1, $item2, $valor2){

		$stmt = Conexion::conectar()->prepare("UPDATE $tabla SET $item1 = :$item1 WHERE $item2 = :$item2");

		$stmt -> bindParam(":".$item1, $valor1, PDO::PARAM_STR);
		$stmt -> bindParam(":".$item2, $valor2, PDO::PARAM_STR);

		if($stmt -> execute()){

			return "ok";
		
		}else{

			return "error";	

		}

		$stmt -> close();

		$stmt = null;

	}

}