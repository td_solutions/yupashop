<?php

class Ruta{

	/*=============================================
	RUTA LADO DEL CLIENTE
	=============================================*/	

	static public function ctrRuta(){

		return "http://localhost:8080/yupashop/";
	
	}

	/*=============================================
	RUTA LADO DEL SERVIDOR
	=============================================*/	

	static public function ctrRutaServidor(){

		return "http://localhost:8080/yupashop/backend/";
	
	}

}