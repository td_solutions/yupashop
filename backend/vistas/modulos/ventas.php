<?php
if($_SESSION["perfil"] != "administrador"){

echo '<script>

  window.location = "inicio";

</script>';

return;

}

?>

<div class="content-wrapper">
  
   <section class="content-header">
      
    <h1>
      Gestor ventas
    </h1>

    <ol class="breadcrumb">

      <li><a href="inicio"><i class="fa fa-dashboard"></i> Inicio</a></li>

      <li class="active">Gestor ventas</li>
      
    </ol>

  </section>

  <section class="content">

    <div class="box"> 

      <div class="box-header with-border">
        
        <?php

        include "inicio/grafico-ventas.php";

        ?>

      </div>

      <div class="box-body">

        <div class="box-tools">

          <a href="vistas/modulos/reportes.php?reporte=compras">
            
              <button class="btn btn-success">Descargar reporte en Excel</button>

          </a>

        </div>

        <br>
        <input type="hidden" id="pakkeApiKey">
        <table class="table table-bordered table-striped dt-responsive tablaVentas" width="100%">
        
          <thead>
            
            <tr>
              
              <th style="width:10px">#</th>
              <th>Producto</th>
              <th>Imagen Producto</th>
              <th>Cliente</th>
              <th>Foto Cliente</th>
              <th>Venta</th>
              <th>Tipo</th>  
              <th>Proceso de envío</th>
              <th>Número de Guia</th>
              <th>No Pedido</th>
              <th>Metodo</th>
              <th>Email</th>
              <th>Telefono</th>
              <th>Dirección</th>
              <th>País</th>
              <th>Fecha</th>

            </tr>

          </thead> 


        </table>


      </div>

    </div>

  </section>

  <!--=====================================
MODAL GENERAR GUIA
======================================-->

<div id="modalGenerarGuia" class="modal fade" role="dialog">
  
  <div class="modal-dialog">
    
    <div class="modal-content">

      <form method="post" enctype="multipart/form-data">
        <!--=====================================
        CABEZA DEL MODAL
        ======================================-->
        
        <div class="modal-header" style="background:#3c8dbc; color:white">
          
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          
          <h4 class="modal-title">Generar Guia (Pakke)</h4>

        </div>

        <!--=====================================
        CUERPO DEL MODAL
        ======================================-->

        <div class="modal-body">
          
          <div class="box-body">
            <input type="hidden" id="nombre_cliente">
            <input type="hidden" id="email_cliente">
            <input type="hidden" id="telefono_cliente">
            <input type="hidden" id="no_pedido">
            <input type="hidden" id="calle_numero">
            <input type="hidden" id="colonia">
            <input type="hidden" id="cp">
            <input type="hidden" id="estado">
            <input type="hidden" id="ciudad">

            <input type="hidden" id="CourierCode">
            <input type="hidden" id="CourierServiceId">
            

            <div class="container-fluid calculoEnvio">
            <!--=====================================
              RENGLON DE PRODUCTOS
              ======================================-->					
					    <div class="formEnvio row">
                <div class="col-md-12 col-sm-12 col-xs-12">	
                  <h4 class="text-center text-muted text-uppercase tituloModalCheckout">Productos</h4>
                  <hr>
                  <div id="div_productos"></div>
                </div>

                <!--=====================================
              RENGLON PROGRESO PAQUETERIA
              ======================================-->	 
              <div id="div_progreso" class="col-md-12 col-sm-12 col-xs-12">	
              <h4 class="text-center text-muted text-uppercase tituloModalCheckout">ESTATUS ENVIO</h4>
                  <hr>
                  
<!--=====================================
VENTANA MODAL PARA DETALLES
======================================-->

			<div class="contenidoCheckout">
      <div class="col-md-6 col-sm-6 col-xs-6">	
					<strong>Número de envio: </strong>
          <span id="txt_no_envio"></span>
          </br>          
					<?php
					echo diaconnombre().', '.date("d").' de '. mesconnombre();
					function mesconnombre(){
						switch (date("n")) {
							case 1:
							return "Enero";
							case 2:
							return "Febrero";
							case 3:
							return "Marzo";
							case 4:
							return "Abril";
							case 5:
							return "Mayo";
							case 6:
							return "Junio";
							case 7:
							return "Julio";
							case 8:
							return "Agosto";
							case 9:
							return "Septiembre";
							case 10:
							return "Octubre";
							case 11:
							return "Noviembre";
							case 12:
							return "Diciembre";
						}
					}
					function diaconnombre(){
						switch (date("N")) {
							case 1:
							return "Lunes";
							case 2:
							return "Martes";
							case 3:
							return "Miércoles";
							case 4:
							return "Jueves";
							case 5:
							return "Viernes";
							case 6:
							return "Sábado";
							case 7:
							return "Domingo";
						}
					}
					?>
          </br>
          <strong>Estatus de entrega: </strong>
					<span id="txt_estatus" class="btn-success"></span>
					</br>
						<span id="h5_hora_entrega">Fecha de entrega: <span id="txt_hora_entrega"></span></span>
					<span id="h5_recibido_por">Recibido por: <span id="txt_recibido_por"></span></span>
          </div>	
          <div class="col-md-6 col-sm-6 col-xs-6">	
					  <h5><strong>Historial de entrega</strong></h5>
					  <span id="historia"></span>
          </div>	
			</div>
</div>

               <!--=====================================
              RENGLON DE DIRECCION DESTINO
              ======================================-->	 
                <div id="div_envio" class="col-md-12 col-sm-12 col-xs-12">	
                  <h4 class="text-center text-muted text-uppercase tituloModalCheckout">Destino</h4>
                  <hr>
                  <table class="table">
                    <thead>
                      <tr>
                      <th scope="col">Dirección</th>
                        <th scope="col">Colonia</th>
                        <th scope="col">Código postal</th>
                        <th scope="col">Ciudad</th>
                        <th scope="col">Estado</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                      <td id="dest_dir"></td>
                        <td id="dest_col"></td>
                        <td id="dest_cp"></td>
                        <td id="dest_ciu"></td>
                        <td id="dest_est"></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>



               <!--=====================================
              RENGLON DE PAKETERIA
              ======================================-->					
					    <div id="div_paqueteria_elegida_pral" class="formEnvio row">
                <div class="col-md-12 col-sm-12 col-xs-12">	
                  <h4 class="text-center text-muted text-uppercase tituloModalCheckout">Paqueteria elegida por el comprador</h4>
                  <hr>
                  <div id="div_paqueteria_elegida">
                  <table class="table">
                    <thead>
                      <tr>
                      <th scope="col">Paqueteria</th>
                        <th scope="col">Servicio</th>
                        <th scope="col">Entrega</th>
                        <th scope="col">Precio</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                      <td id="pakke_nombre"></td>
                        <td id="pakke_servicio"></td>
                        <td id="pakke_entrega"></td>
                        <td id="pakke_precio"></td>
                      </tr>
                    </tbody>
                  </table>
                  </div>                   
               </div>
                </div>              
              <!--=====================================
              RENGLON DE MEDIDAS
              ======================================-->					
					    <div id="div_medidas" class="formEnvio row">
                <h4 class="text-center text-muted text-uppercase tituloModalCheckout">Cálculo de envío</h4>
                <hr>
                <!--=====================================
                LONGITUD DEL PAQUETE
                ======================================-->
                <div class="col-md-3 col-sm-3 col-xs-12">							
                  <div class="form-group">
                    <label for="pakke_length">Longitud</label>
                    <input type="text" class="form-control input-sm soloNumeros" id="pakke_length" placeholder="0.0 m">
                  </div>
                </div>
                <!--=====================================
                ANCHO DEL PAQUETE
                ======================================-->
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="form-group">
                    <label for="pakke_width">Ancho</label>
                    <input type="text" class="form-control input-sm soloNumeros" id="pakke_width" placeholder="0.0 m">
                  </div>
                </div>
                <!--=====================================
                ALTO DEL PAQUETE
                ======================================-->
                <div class="col-md-3 col-sm-3 col-xs-12">
                  <div class="form-group">
                    <label for="pakke_height">Alto</label>
                    <input type="text" class="form-control input-sm soloNumeros" id="pakke_height" placeholder="0.0 m">
                  </div>						
                </div>
                <!--=====================================
                PESO DEL PAQUETE
                ======================================-->
                <div class="col-md-3 col-sm-3 col-xs-12">							
                  <div class="form-group">
                    <label for="pakke_weight">Peso</label>
                    <input type="text" class="form-control input-sm soloNumeros" id="pakke_weight" placeholder="0.0 kg" disabled>
                  </div>
                </div>
                <!--=====================================
              RENGLON DE CP
              ======================================-->				
              <div id="div_cp" class="formEnvio row">
                <div class="col-md-6 col-sm-6 col-xs-6">	                  
                  <div class="form-group">
                    <label for="pakke_length">Código postal remitente</label>
                    <input type="text" class="form-control input-sm soloNumeros" value="36670" maxlength="5" id="pakke_zip_from">
                  </div>
               </div>
                </div>
                 <!--=====================================
              RENGLON DE CONTENIDO
              ======================================-->				
              <div id="div_cntd" class="formEnvio row">
                <div class="col-md-12 col-sm-12 col-xs-12">	                  
                  <div class="form-group">
                    <label for="pakke_length">*Especifique el contenido del paquete</label>
                    <input type="text" class="form-control input-sm" maxlength="25" id="contenido">
                  </div>
               </div>
                </div>
                   <!--=====================================
              RENGLON DE DIRECCION DE RECOLECCO
              ======================================-->				
              <div id="div_cntd" class="formEnvio row">
                <div class="col-md-12 col-sm-12 col-xs-12">	                  
                  <div class="form-group">
                    <label for="pakke_length">*Especifique la direccion de recolección (30 caracteres max.)</label>
                    <input type="text" class="form-control input-sm" maxlength="29" id="direccion_recoleccion">
                  </div>
               </div>
                </div>
                <!--=====================================
              RENGLON DE DIRECCION DE RECOLECCO
              ======================================-->				
              <div id="div_cntd" class="formEnvio row">
                <div class="col-md-12 col-sm-12 col-xs-12">	                  
                  <div class="form-group">
                    <label for="pakke_length">*Especifique la direccion de recolección 2 (10 caracteres max.)</label>
                    <input type="text" class="form-control input-sm" maxlength="10" id="direccion_recoleccion_2">
                  </div>
               </div>
                </div>
                   <!--=====================================
              RENGLON DE COLONIA
              ======================================-->				
              <div id="div_cntd" class="formEnvio row">
                <div class="col-md-12 col-sm-12 col-xs-12">	                  
                  <div class="form-group">
                    <label for="pakke_length">*Colonia de recolección</label>
                    <input type="text" class="form-control input-sm" maxlength="29" id="colonia_recoleccion">
                  </div>
               </div>
                </div>
              <!--=====================================
              FIN DE RENGLON
              ======================================-->              
            </div>
          </div>
        </div>
        <!--=====================================
        PIE DEL MODAL
        ======================================-->
        <div class="modal-footer">          
          <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
          <button type="submit" id="btn_generar_guia_pakke" class="btn btn-primary">Generar guia</button>
          <button type="submit" id="btn_terminar_pedido" class="btn btn-success">Terminar pedido</button>
        </div>
      </form>
    </div>
  </div>
</div>

</div>

<!--=====================================
MARIPOSITA
======================================-->

<div class="modal fade modalFormulario" data-backdrop="static" data-keyboard="false" id="modalSpinner" role="dialog" style="z-index: 99999999999;">
<div class="modal-body" style="color:white;position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%) scale(0.740741) translateZ(0px); zoom: 1;"> 
    <center>
      <span id="spinner" class="fa fa-spinner fa-pulse fa-3x fa-fw">
    </center>
  </div>
</div>