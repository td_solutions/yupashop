var endPointApi = "https://seller.pakke.mx/api/v1";
/*=============================================
CARGAR LA TABLA DINÁMICA DE VENTAS
=============================================*/

// $.ajax({

// 	url:"ajax/tablaVentas.ajax.php",
// 	success:function(respuesta){

// 		console.log("respuesta", respuesta);

// 	}

// })

cargarTablaVentas();
$("#colonia_recoleccion").val("Villas Carr. Libramiento Norte Irapuato-Leon s/n Km 1.5, Ejido las Malvas, 36826 Irapuato, Gto.");
$("#direccion_recoleccion").val("Plaza de las fuentes 6273");
$("#direccion_recoleccion_2").val("Int. 49");
/*=============================================
PROCESO DE ENVÍO
=============================================*/

$(".tablaVentas tbody").on("click", ".btnEnvio", function() {

    var peso = 0.0;
    var longitud = 0.0;
    var alto = 0.0;
    var ancho = 0.0;
    var etapa = $(this).attr("etapa");
    var no_guia = "0";
    if (etapa == 1) {
        $("#div_progreso").css('display', 'none');
        $("#div_envio").css('display', 'inline');
        $("#div_medidas").css('display', 'inline');
        $("#div_cp").css('display', 'inline');
        $("#div_paqueteria_elegida_pral").css('display', 'inline');
        $("#btn_generar_guia_pakke").css('display', 'inline');
        $("#btn_terminar_pedido").css('display', 'none');
    } else if (etapa == 2) {
        $("#div_progreso").css('display', 'inline');
        $("#div_envio").css('display', 'none');
        $("#div_medidas").css('display', 'none');
        $("#div_cp").css('display', 'none');
        $("#div_paqueteria_elegida_pral").css('display', 'none');
        $("#btn_generar_guia_pakke").css('display', 'none');
        $("#btn_terminar_pedido").css('display', 'inline');
        no_guia = $(this).attr("no_guia");
    } else
        return;

    obtenerPakkeApiKey();

    var nombre_cliente = $(this).attr("nombre_cliente");
    var email_cliente = $(this).attr("email_cliente");
    var telefono_cliente = $(this).attr("telefono_cliente");

    $("#nombre_cliente").val(nombre_cliente);
    $("#email_cliente").val(email_cliente);
    $("#telefono_cliente").val(telefono_cliente);
    //modal
    $("#modalGenerarGuia").modal("show");
    var no_pedido = $(this).attr("no_pedido");
    $("#no_pedido").val(no_pedido);
    var datos = new FormData();
    datos.append("no_pedido", no_pedido);
    datos.append("datos_pedido", "si");

    $.ajax({

        url: "ajax/pedidos.ajax.php",
        method: "POST",
        async: true,
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta) {
            var datos = JSON.parse(respuesta)[0];

            $("#dest_dir").html(datos.calle_numero);
            $("#dest_col").html(datos.colonia);
            $("#dest_est").html(datos.estado);
            $("#dest_ciu").html(datos.ciudad);
            $("#dest_cp").html(datos.cp);


            $("#CourierServiceId").val(datos.Pakke_CourierServiceId);
            $("#CourierCode").val(datos.Pakke_CourierCode);

            $("#pakke_nombre").html(datos.Pakke_CourierName);
            $("#pakke_servicio").html(datos.Pakke_CourierServiceName);
            $("#pakke_entrega").html(datos.Pakke_DeliveryDays);
            $("#pakke_precio").html(datos.Pakke_TotalPrice);
        }
    });
    if (no_guia != "undefined" && no_guia != "0")
        Consultar_Detalles_Pakke(no_guia, $("#CourierCode").val());

    datos = new FormData();
    datos.append("no_pedido", no_pedido);
    datos.append("lista_productos", "si");

    $.ajax({

        url: "ajax/pedidos.ajax.php",
        method: "POST",
        data: datos,
        async: true,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta) {
            var datos = JSON.parse(respuesta);
            $("#div_productos").html("");
            $.each(datos, function(i, item) {
                $("#div_productos").append('<div class="form-group"><span precio_prod="' + item.precio + '" img_prod="' + item.portada + '"> ' + item.titulo + ' Talla:' + item.talla + ' Color:' + item.color + '<img src="' + item.portada + '" alt="vista previa" height="42" width="42"></span></div>');
                $("#contenido").val(item.titulo);
                peso += parseFloat(item.peso);
                ancho += parseFloat(item.ancho);
                alto += parseFloat(item.alto);
                longitud += parseFloat(item.longitud);
            });
            $("#pakke_weight").val(Math.ceil(peso));
            $("#pakke_height").val(Math.ceil(alto));
            $("#pakke_width").val(Math.ceil(ancho));
            $("#pakke_length").val(Math.ceil(longitud));
        },
        error: function(res) {
            swal({
                title: "Error",
                text: res.responseText,
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Ok",
                closeOnConfirm: false
            })
        }
    });

    /* var idVenta = $(this).attr("idVenta");
	var etapa = $(this).attr("etapa");

	var datos = new FormData();
 	datos.append("idVenta", idVenta);
  	datos.append("etapa", etapa);

  		$.ajax({

  		 url:"ajax/ventas.ajax.php",
  		 method: "POST",
	  	data: datos,
	  	cache: false,
      	contentType: false,
      	processData: false,
      	success: function(respuesta){ 
      	    
      	  console.log("respuesta", respuesta);

      	} 	 

  	});

  	if(etapa == 1){
	
  		$(this).addClass('btn-warning');
  		$(this).removeClass('btn-danger');
  		$(this).html('Enviando el producto');
  		$(this).attr('etapa', 2);

  	}

	if(etapa == 2){
	
  		$(this).addClass('btn-success');
  		$(this).removeClass('btn-warning');
  		$(this).html('Producto entregado');
	
	  } */
});


$("#btn_generar_guia_pakke").click(function() {
    event.preventDefault();
    if ($("#pakke_zip_from").val().length > 0) {
        generar_guia();
    } else {
        swal({
            type: "warning",
            title: "Complete los datos requeridos",
            showConfirmButton: true,
            confirmButtonText: "Ok"
        })
    }
});

$("#btn_terminar_pedido").click(function() {
    event.preventDefault();
    terminar_pedido();
});

function terminar_pedido() {
    var no_pedido = $("#no_pedido").val();
    if (no_pedido != 'undefinied') {

        datos = new FormData();
        datos.append("no_pedido", no_pedido);
        datos.append("terminado", "si");

        $.ajax({
            url: "ajax/pedidos.ajax.php",
            method: "POST",
            data: datos,
            async: false,
            cache: false,
            contentType: false,
            processData: false,
            success: function(respuesta) {
                //var datos = JSON.parse(respuesta);
                swal({
                    type: "success",
                    title: "Pedido finalizado",
                    showConfirmButton: true,
                    confirmButtonText: "Cerrar"
                }).then(function(result) {
                    if (result.value) {
                        $(".tablaVentas").DataTable().destroy();
                        cargarTablaVentas();
                        $("#modalGenerarGuia").modal('hide');

                    }
                });
            }
        });

    }

}

function generar_guia() {
    $('#modalSpinner').modal('toggle');
    testPakkeApi($("#pakkeApiKey").val());
}


function obtenerPakkeApiKey() {
    var ruta = "../ajax/pakke.ajax.php";
    var datos = new FormData();
    var resultado;
    datos.append("accion", "obtenerPakkeApiKey");
    $.ajax({
        url: ruta,
        method: "POST",
        async: false,
        data: datos,
        cache: false,
        contentType: false,
        processData: false,
        success: function(respuesta) {
            resultado = eval(respuesta)[0].pakke_api_key;
            $("#pakkeApiKey").val(resultado);
        }

    });
    return resultado;
}



function testPakkeApi(PakkeApiKey) {

    try {

        var CourierCode = $("#CourierCode").val();
        var CourierServiceId = $("#CourierServiceId").val();
        var ZipCodeTo = $("#dest_cp").html();
        var ZipCodeFrom = $("#pakke_zip_from").val();
        var CourierName = $("#pakke_nombre").html();
        var nombre_cliente = $("#nombre_cliente").val();
        var email_cliente = $("#email_cliente").val();
        var telefono_cliente = $("#telefono_cliente").val();
        var Contenido = $("#contenido").val();
        var Contenido_Imagenes = "";
        debugger;
        $("#div_productos span").each(
            function() {
                Contenido_Imagenes += "<tr><td align='center' valign='top' style='width:150px;height:115px'>" +
                    "<img border='0' src='" + $(this)[0].lastChild.currentSrc + "' style='width:120px;height;80px'>" +
                    "</td><td valign='top' style='color:#666;padding:10px 10px 0 10px;width:330px;height:115px'>" +
                    "<span style='font:14px Arial,sans-serif;text-decoration:none;color:#006699'>" + $(this)[0].textContent + "</span>" +
                    "</td><td align='left' valign='top' style='font:14px Arial,san-serif;padding:10px 0 0 0;width:80px;height:115px'><strong>$" + $(this)[0].attributes[0].textContent + "</strong></td>"
                "</tr><table></td></tr><table>";
            });
        //Debido a que la libreria solo acepta 25 caracteres en este campo
        if (Contenido.length > 25)
            Contenido = Contenido.substring(0, 24);

        parcel = new Object();
        parcel.Length = $("#pakke_length").val();
        parcel.Width = $("#pakke_width").val();
        parcel.Height = $("#pakke_height").val();
        parcel.Weight = $("#pakke_weight").val();

        AddressFrom = new Object();
        AddressFrom.ZipCode = ZipCodeFrom;
        AddressFrom.State = "Guanajuato";
        AddressFrom.City = "Irapuato";
        AddressFrom.Neighborhood = $("#colonia_recoleccion").val();
        AddressFrom.Address1 = $("#direccion_recoleccion").val();
        AddressFrom.Address2 = $("#direccion_recoleccion_2").val();
        AddressFrom.Residential = false;

        AddressTo = new Object();
        AddressTo.ZipCode = ZipCodeTo;
        AddressTo.State = $("#dest_est").html();
        AddressTo.City = $("#dest_ciu").html();
        AddressTo.Neighborhood = $("#dest_col").html();

        var direccion = $("#dest_dir").html() + " " + $("#dest_col").html();
        AddressTo.Address1 = direccion;
        if (direccion.length > 30) {
            AddressTo.Address1 = direccion.substring(0, 30);
            AddressTo.Address2 = direccion.substring(30, direccion.length);
        }


        AddressTo.Residential = false;
        Sender = new Object();
        Sender.Name = "Yupashop";
        Sender.CompanyName = "YUPASHOP SA DE CV";
        Sender.Phone1 = "4621732966";
        //Sender.Phone2 = "9992009999";
        Sender.Email = "tienda@yupashop.com";

        Recipient = new Object();
        Recipient.Name = nombre_cliente;
        //Recipient.Phone1 = "0000000000";
        Recipient.Phone1 = telefono_cliente;
        Recipient.Email = email_cliente;

        var objDatos = JSON.stringify({ 'Content': Contenido, 'CourierCode': CourierCode, 'CourierServiceId': CourierServiceId, 'AddressFrom': AddressFrom, 'AddressTo': AddressTo, 'Parcel': parcel, 'Sender': Sender, 'Recipient': Recipient });
        Contenido = "";
        $.ajax({
            url: "https://seller.pakke.mx/api/v1/Shipments",
            //url:"ajax/pedidos.ajax.prueba.php",
            type: "POST",
            data: objDatos,
            async: false,
            contentType: 'application/json',
            beforeSend: function(xhr) {
                // Authorization header
                xhr.setRequestHeader("Authorization", PakkeApiKey);
            },
            //Authorization:PakkeApiKey,
            dataType: "json",
            success: function(respuesta) {

                var pakke_guia = respuesta.TrackingNumber;
                var pakke_email_usuario = respuesta.Recipient.Email;
                var no_pedido = $("#no_pedido").val();
                datos = new FormData();
                datos.append("no_pedido", no_pedido);
                datos.append("TrackingNumber", pakke_guia);
                datos.append("emailUsuario", pakke_email_usuario);
                datos.append("contenidoImagenes", Contenido_Imagenes);
                datos.append("CourierName",CourierName);

                $.ajax({
                    url: "ajax/pedidos.ajax.php",
                    method: "POST",
                    data: datos,
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(respuesta) {
                        //var datos = JSON.parse(respuesta);
                        swal({
                            type: "success",
                            title: "Guia generada No." + pakke_guia,
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        }).then(function(result) {
                            if (result.value) {
                                $(".tablaVentas").DataTable().destroy();
                                cargarTablaVentas();
                                $("#modalGenerarGuia").modal('hide');

                            }
                        });
                    }
                });
            },
            error: function(res) {
                swal({
                    title: "Error",
                    text: res.responseText,
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Ok",
                    closeOnConfirm: false
                })
            },
            complete: function() {
                $('#modalSpinner').modal('toggle');
            }
        });
    } catch (e) {
        $('#modalSpinner').modal('toggle');
        swal({
            title: "Technical Report",
            text: e,
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false
        });
    }
}

function cargarTablaVentas() {
    $(".tablaVentas").DataTable({
        "ajax": "ajax/tablaVentas.ajax.php",
        "deferRender": true,
        "retrieve": true,
        "processing": true,
        "language": {

            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ningún dato disponible en esta tabla",
            "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_",
            "sInfoEmpty": "Mostrando registros del 0 al 0 de un total de 0",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }

        }


    });
}

function Consultar_Detalles_Pakke(p_guia, p_courier_code) {
    $("#txt_no_envio").html(p_guia);
    $("#historia").html("");
    try {
        var ApiKeyPakke = $("#pakkeApiKey").val();
        if (ApiKeyPakke != null) {
            $.ajax({
                url: endPointApi + "/Shipments/tracking?courierCode=" + p_courier_code + "&trackingNumber=" + p_guia,
                type: "GET",
                async: false,
                contentType: 'application/json',
                beforeSend: function(xhr) {
                    /* Authorization header */
                    xhr.setRequestHeader("Authorization", ApiKeyPakke);
                },
                //Authorization:PakkeApiKey,
                dataType: "json",
                success: function(respuesta) {

                    var objeto = JSON.parse(JSON.stringify(respuesta));
                    $('#txt_paqueteria').html(objeto[0].CourierServiceId); //CourierServiceId
                    $('#txt_estatus').html(objeto[0].TrackingStatus);
                    if (objeto[0].TrackingStatus == 'DELIVERED') {
                        $('#txt_hora_entrega').html(formato_fecha(objeto[0].ReceivedAt));
                        $('#txt_recibido_por').html(objeto[0].ReceivedBy);
                        $('#h5_hora_entrega').css('display', 'block');
                        $('#h5_recibido_por').css('display', 'block');
                    }

                    var obj_historia = JSON.parse(JSON.stringify(objeto[0].History));
                    obj_historia.forEach(funcionForEach);

                    function funcionForEach(item, index) {
                        $("#historia").append("<strong><u>" + formato_fecha(item.Date) + "</strong></u></br>" + item.Details + "</br>");
                    }
                },
                error: function(res) {
                    swal({
                        title: "Error",
                        text: eval(res.responseJSON).error.message,
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        closeOnConfirm: false
                    })
                },
                complete: function() {
                    //$('#modalSpinner').modal('toggle');
                }
            });
        }
    } catch (e) {
        swal({
            title: "Technical Report",
            text: e,
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            closeOnConfirm: false
        })
    }
}

function formato_fecha(fecha) {
    var monthNames = [
        "Enero", "Februaro", "Marzo",
        "Abril", "Mayo", "Junio", "Julio",
        "Agosto", "Septiembre", "Octubre",
        "Noviembre", "Diciembre"
    ];
    var dayNames = [
        "Lunes", "Martes", "Miercoles",
        "Jueves", "Viernes", "Sabado", "Domingo"
    ];
    var dt_fecha = new Date(fecha);
    var day = dt_fecha.getDay();
    var dia = dt_fecha.getDate()
    var monthIndex = dt_fecha.getMonth();
    var year = dt_fecha.getFullYear();
    var hora = dt_fecha.getHours();
    var minutos = dt_fecha.getMinutes();
    var segundos = dt_fecha.getSeconds();

    return dayNames[day] + ', ' + dia + ' de ' + monthNames[monthIndex] + ' ' + year + ' ' + hora + ':' + minutos;
}